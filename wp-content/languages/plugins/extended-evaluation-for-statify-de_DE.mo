��    =        S   �      8     9     E     Q     ]  C   i     �     �     �  
   �     �     �  V   �  	   ;     E     M     U     l     z     �  J   �     �     �     	  t        �  	   �  	   �  
   �  	   �     �     �  N        U     h  
   y     �  �   �     L	     P	     T	     Y	     _	     d	  	   q	     {	     �	     �	     �	     �	  &   �	     �	  
   �	  	   
  	   
  
   
  	   '
  	   1
     ;
     >
  	   D
  6  N
  
   �  
   �  
   �  
   �  M   �     �               &     /     8  p   D  
   �     �     �     �     �            T   /  
   �     �     �  �   �  !   Z  
   |     �     �     �  "   �     �  L   �     (     <  
   U  !   `  �   �     7     =     A     H     P     U     h     u  "   �     �     �     �  &   �                    +     8     E     Q     ]     a     g              -              6           9                               3   $          /             
          <             1      	   !      #       *                            2   :             8      +      7   0   =   5               %   )             ,      .   (          "   &   '      4   ;    1st quarter 2nd quarter 3rd quarter 4th quarter Activation Error: Statify – Extended Evaluation requires Statify! Average Content Daily Views Date range End date Export Extended evaluation for the compact, easy-to-use and privacy-compliant Statify plugin. Home Page Maximum Minimum Monthly / Yearly Views Monthly Views Most Popular Content No data available. No valid date period set. Please enter a valid start and a valid end date! Overview Overview and Years Patrick Robrecht Per default the views of all posts are shown. To restrict the evaluation to one post/page, enter their path or name. Popular Content and Post Types Post Type Post/Page Proportion Referrers Referrers from other websites Referring Domain Restrict date period: Please enter start and end date in the YYYY-MM-DD format Select date period Select post/page Start date Statify – Extended Evaluation Statify – Extended Evaluation requires the plugin Statify which has to be installed and activated! Please install and activate Statify before activating this plugin! Sum URL View Views Year Yearly Views all posts custom default (all the time) for %1$s %2$s from https://patrick-robrecht.de/ https://patrick-robrecht.de/wordpress/ last 28 days last month last week last year this month this week this year to today yesterday PO-Revision-Date: 2019-05-05 18:48:44+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: de
Project-Id-Version: Plugins - Statify – Extended Evaluation - Stable (latest release)
 1. Quartal 2. Quartal 3. Quartal 4. Quartal Fehler bei Aktivierung: Statify – Erweiterte Auswertung benötigt Statify!  Durchschnitt Inhalte Tägliche Aufrufe Zeitraum Enddatum Exportieren Erweiterte Auswertung für das kompakte, anwendungsfreundliche und datenschutzkonforme Statistik-Plugin Statify. Startseite Maximum Minimum Monatliche / jährliche Aufrufe Monatliche Aufrufe Beliebteste Inhalte Keine Daten verfügbar. Kein gültiger Zeitraum ausgewählt. Bitte gib ein gültiges Start- und Enddatum an. Überblick Überblick und Jahre Patrick Robrecht Standardmäßig werden die Aufrufe für alle Inhalte angezeigt. Um die Auswertung auf einen Beitrag/eine Seite zu beschränken, gib dessen/deren Pfad oder Namen an. Beliebte Inhalte und Inhaltstypen Inhaltstyp Beitrag/Seite Anteil Verweise Verweise von anderen Webauftritten Verweisende Domain Zeitraum eingrenzen: bitte gib Start- und Enddatum im Format JJJJ-MM-TT ein. Zeitraum auswählen Beitrag/Seite auswählen Startdatum Statify – Erweiterte Auswertung Statify – Erweiterte Auswertung benötigt das Plugin Statify, das installiert und aktiviert sein muss. Bitte installiere und aktiviere Statify, bevor du dieses Plugin aktivierst. Summe URL Aufruf Aufrufe Jahr Jährliche Aufrufe alle Inhalte benutzerdefiniert Standard (vollständiger Zeitraum) für %1$s %2$s vom https://patrick-robrecht.de/ https://patrick-robrecht.de/wordpress/ letzte 28 Tage letzter Monat letzte Woche letztes Jahr dieser Monat diese Woche dieses Jahr bis heute gestern 