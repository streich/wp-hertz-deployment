��          |      �            !  	   ?     I     c  
   ~     �     �     �     �     �     �  I  �  c  D  	   �     �      �  
   �     �            #   5     Y     v                
      	                                     <label>Hide <b>Meow Apps</b> Menu</label><br /><small>Hide Meow Apps menu and all its components, for a cleaner admin. This option will be reset if a new Meow Apps plugin is installed. <b>Once activated, an option will be added in your General settings to display it again.</b></small> Dashboard Delete with Media Cleaner Invalid Regular-Expression Jordy Meow Media Cleaner Orphan Retina Orphan WebP Restore with Media Cleaner Trash with Media Cleaner https://meowapps.com PO-Revision-Date: 2020-09-01 08:37:16+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: de
Project-Id-Version: Plugins - Media Cleaner &#8211; Clean &amp; Optimize Space - Stable (latest release)
 <label>Verberge<b>Meow Apps</b> Menu</label><br /><small>Verbirgt für eine sauberere Verwaltung das Meow Apps Menü und alle seine Komponenten. Diese Option wird zurückgesetzt, wenn ein neues Meow Apps Plugin installiert wird. <b>Sobald aktiviert, wird in deinen allgemeinen Einstellungen eine Option hinzugefügt, um sie wieder anzuzeigen.</b></small>  Dashboard Löschen mit Media Cleaner  Ungültiger regulärer Ausdruck  Jordy Meow Media Cleaner Verwaiste Retina-Datei Verwaiste WebP-Datei  Wiederherstellen mit Media Cleaner  Entsorgen mit Media Cleaner  http://meowapps.com 