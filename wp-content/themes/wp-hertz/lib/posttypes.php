<?php
// Register Custom Taxonomy
function people_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Aufgabenbereich', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Aufgabenbereich', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Aufgabenbereich', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'people', array( 'post' ), $args );

}
flush_rewrite_rules();
// Hook into the 'init' action
add_action( 'init', 'people_taxonomy', 0 );

// Register Custom Taxonomy
function show_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Sendungskategorien', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Sendungskategorie', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Sendungskategorie', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' 					 => array('slug' => 'ressort')
	);
	register_taxonomy( 'show', array( 'post' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'show_taxonomy', 0 );

flush_rewrite_rules();



function create_post_type() {
	register_post_type( 'shows',
		array(
			'labels' => array(
			'menu_name' => __( 'Programm', 'text_domain' ),
			'name' => __( 'Artikel' ),
			'singular_name' => __( 'Programm' )),
			'taxonomies'  => array( 'show' ),
			'supports'    => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'public' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
			'hierarchical' => false,
			'menu_icon' => 'dashicons-admin-collapse',
			'query_var' => true,
      'capability_type' => 'post',
			'rewrite' => array('slug' => 'sendung')
			// 'rewrite' => array('slug' => 'shows')
		)
	);
 
flush_rewrite_rules();

	// Events Posttype
	register_post_type( 'events',
		array(
			'labels' => array(
			'name' => __( 'Veranstaltungen' ),
			'singular_name' => __( 'Veranstaltung' )),
			'public' => true,
			'menu_icon' => 'dashicons-location',
			'public' => true,
			'publicly_queryable' => true,
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'capability_type' => 'post',
			'has_archive' => true,
			'rewrite' => array('slug' => 'veranstaltung')
		)
	);
	flush_rewrite_rules();
	// Messages Posttype
	register_post_type( 'messages',
		array(
			'labels' => array(
			'name' => __( 'Meldungen' ),
			'singular_name' => __( 'Meldungen' )),
			'public' => true,
			'menu_icon' => 'dashicons-megaphone',
			'capability_type' => 'post',
			'publicly_queryable' => true,
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'has_archive' => true,
			'rewrite' => array('slug' => 'meldung')
		)
	);

	flush_rewrite_rules();

	// News Posttype
	register_post_type( 'news',
		array(
			'labels' => array(
			'name' => __( 'Nachrichten' ),
			'singular_name' => __( 'Nachricht' )),
      'menu_icon' => 'dashicons-format-aside',
			'public' => true,
      'capability_type' => 'post',
			'publicly_queryable' => true,
			'hierarchical' => false,
			'supports'    => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'has_archive' => true,
			'rewrite' => array('slug' => 'nachrichten')
		)
	);
	flush_rewrite_rules();

	// Linkschleuder Posttype
	register_post_type( 'linklist',
		array(
		  'labels' => array(
				'menu_name' => __( 'Linkschleuder', 'text_domain' ),
				'name' => 'Linkschleuder',
				'singular_name' => 'Link'),
			'publicly_queryable' => true,
			'hierarchical' => false,
			'supports' => array( 'title','author'),
      'capability_type' => 'post',
      'menu_icon' => 'dashicons-admin-links',
			'public' => true,
			'show_in_menu' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'linklist')
		)
	);
	flush_rewrite_rules();
	// Podcasts Posttype
/*
	register_post_type( 'podcasts',
		array(
			'labels' => array(
			'name' => __( 'Podcasts' ),
			'singular_name' => __( 'Podcast' )),
			'taxonomies'  => array( 'show' ),
			'publicly_queryable' => true,
			'hierarchical' => false,
			'supports' => array( 'title','editor', 'author', 'excerpt','thumbnail'),
      'capability_type' => 'post',
      'menu_icon' => 'dashicons-format-audio',
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'podcast')
		)
	);
	flush_rewrite_rules();
*/
	// Pepole Posttype
	register_post_type( 'people',
		array(
			'labels' => array(
			'name' => __( 'Menschen' ),
			'singular_name' => __( 'Menschen' )),
			// 'taxonomies'   => array( 'people' ),
   		'menu_icon' => 'dashicons-groups',
 			'publicly_queryable' => true,
			'hierarchical' => false,
   		'supports' => array( 'title', 'editor',  'thumbnail', 'excerpt'),
			'public' => true,
    	'capability_type' => 'post',
			'has_archive' => true,
			'rewrite' => array('slug' => 'menschen')
		)
	);
	flush_rewrite_rules();

}

add_action( 'init', 'create_post_type' );
flush_rewrite_rules();

// function category(){
// 	echo "<script>alert('hello');</script>";
// 	var_dump("aofweofjqwogjLorem ipsum Commodo mollit in nisi elit nostrud sed occaecat officia dolore occaecat irure ex enim commodo aliquip reprehenderit esse elit qui Duis velit esse in laboris esse in adipisicing cillum mollit velit qui incididunt est nulla amet elit dolore veniam qui fugiat ullamco in deserunt cupidatat occaecat anim laboris labore occaecat elit do sit cillum est labore voluptate consectetur dolor Duis veniam sunt nostrud adipisicing ullamco occaecat sed in culpa ut sed minim consequat pariatur veniam aliqua in culpa qui in sunt deserunt culpa culpa velit consectetur sint ex exercitation cupidatat est voluptate ad consequat consectetur aliqua consectetur non cupidatat cillum dolore veniam amet do ad sit proident ullamco exercitation dolore nulla occaecat deserunt fugiat minim fugiat Duis velit in incididunt esse ex reprehenderit eu exercitation veniam et voluptate dolore exercitation esse dolore tempor cupidatat adipisicing anim mollit voluptate pariatur in esse ullamco sed ut ex officia pariatur labore eu exercitation sit dolore in exercitation nisi commodo consequat proident id esse et enim esse eu ad cillum exercitation quis ut sit id do Duis cillum esse sit aliquip sed sit nisi enim anim sunt Excepteur dolor non ea.");
// }

// add_action( 'edit_category', 'category' );