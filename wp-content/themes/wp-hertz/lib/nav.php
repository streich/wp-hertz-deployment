<?php
/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * Roots_Nav_Walker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 */


// class Roots_Nav_Walker extends Walker_Nav_Menu {
//     private $column_limit = 5;
//     private $show_widget = false;
//     private $column_count = 0;
//     static $li_count = 0;


//   function check_current($classes) {
//     return preg_match('/(current[-_])|active|dropdown/', $classes);
//   }

//   function start_lvl(&$output, $depth = 0, $args = array()) {
//     $output .= "\n<ul class=\"dropdown-menu\">\n";
//   }

//   function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
//     $item_html = '';
//             $classes = empty($item->classes) ? array() : (array) $item->classes;
//         $item_id = $item->ID;
//     parent::start_el($item_html, $item, $depth, $args);

//     if ($item->is_dropdown && ($depth === 0)) {
//       $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
//       $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
//     }
//     elseif (stristr($item_html, 'li class="divider')) {
//       $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
//     }
//     elseif (stristr($item_html, 'li class="dropdown-header')) {
//       $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
//     }

//     $item_html = apply_filters('roots/wp_nav_menu_item', $item_html);
//     $output .= $item_html;


//   }

//   function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
//     $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

//     if ($element->is_dropdown) {
//       $element->classes[] = 'dropdown';
//     }

//     parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
//   }
// }


// /**
//  * Clean up wp_nav_menu_args
//  *
//  * Remove the container
//  * Use Roots_Nav_Walker() by default
//  */
// function roots_nav_menu_args($args = '') {
//   $roots_nav_menu_args['container'] = false;

//   if (!$args['items_wrap']) {
//     $roots_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
//   }

//   if (!$args['depth']) {
//     $roots_nav_menu_args['depth'] = 2;
//   }

//   if (!$args['walker']) {
//     $roots_nav_menu_args['walker'] = new Roots_Nav_Walker();
//   }

//   return array_merge($args, $roots_nav_menu_args);
// }
// add_filter('wp_nav_menu_args', 'roots_nav_menu_args');







/**
*  supports multiple columns
**/
class megaMenuWalker extends Walker_Nav_Menu {
    private $column_limit = 4;
    private $show_widget = false;
    private $column_count = 0;
    private $is_mega = false;
    static $li_count = 0;
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $item_id = $item->ID;
        if ($item->is_dropdown && ($depth === 0)) {
          $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
          $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
        }
        elseif (stristr($item_html, 'li class="divider')) {
          $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
        }
        elseif (stristr($item_html, 'li class="dropdown-header')) {
          $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
        }
        if ($depth == 0) {
            self::$li_count = 0;
        }
        if ($depth == 0 && in_array("widget", $classes)) {
            $this->show_widget = true;
            $this->column_count++;
        }
        if ($depth == 1 && self::$li_count == 1) {
            $this->column_count++;
        }

        if ($depth == 1 && in_array("break", $classes) && self::$li_count != 1 && $this->column_count < $this->column_limit) {
            $output .= "</ul><ul class=\"sub-menu\">";
            $this->column_count++;
        }
            $output .= $item_html;

        $class_names = join(" ", apply_filters("nav_menu_css_class", array_filter($classes), $item)); // set up the classes array to be added as classes to each li
        $class_names = " class=\"" . esc_attr($class_names) . "\"";

        if ($depth == 1 && in_array("headline", $classes)){
            $output .= sprintf(
                "<li id=\"menu-item-%s\"%s>%s",
                $item_id,
                $class_names,
                $item->title
            );
        }elseif ($item->is_dropdown && ($depth === 0)) {

            $output .= sprintf(
                "<li id=\"menu-item-%s\" %s><a  class=\"dropdown-toggle\" data-toggle=\"dropdown\" data-target=\"#\" href=\"%s\">%s <b class=\"caret\"></b></a>",
                $item_id,
                $class_names,
                $item->url,
                $item->title
            );
        }else{
             $output .= sprintf(
                "<li id=\"menu-item-%s\" %s><a href=\"%s\">%s </a>",
                $item_id,
                $class_names,
                $item->url,
                $item->title
            );

        }
        self::$li_count++;
    }
    // function start_lvl_muliticolumn(&$output, $depth = 0, $args = array()) {
    //     if ($depth == 0) {
    //         $output .= "<section><div class='container'><div class='row'>";
    //     }
    //     $output .= "<ul class=\"sub-menu col-md-3\">";
    // }
    function start_lvl(&$output, $depth = 0, $args = array()) {
        if ($depth == 0) {
            $output .= "<section class=\"dropdown-menu\"><div class=\"dropdown-menu-inner\">";
        }
        $output .= "\n<ul class=\"sub-menu\">\n";

    }
    function end_lvl(&$output, $depth = 0, $args = array()) {
        // $output .= "</ul>";
        $output .= "</ul>";
        if ($depth == 0) {
            if ($this->show_widget) {
                ob_start();
                dynamic_sidebar("Navigation Callout");
                $widget = ob_get_contents();
                ob_end_clean();
                $output .= $widget;
                $this->show_widget = false;
            }

            $output .= "</div></section>";
        }
    }
    function end_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        if ($depth == 0 && $this->column_count > 0) {
            $this->column_count = 0;
        }
        $output .= "</li>";
    }
    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

        if ($element->is_dropdown) {
          $element->classes[] = 'dropdown';
        }

    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }
}

// add mega-menu-columns-# classes
function add_column_number($items) {
    static $column_limit = 3;
    static $post_id = 0;
    static $x_key = 0;
    static $column_count = 0;
    static $li_count = 0;
    $tmp = array();
    foreach($items as $key => $item) {
        if (0 == $item->menu_item_parent) {
            $x_key = $key;
            $post_id = $item->ID;
            $column_count = 0;
            $li_count = 0;
            if (in_array("widget", $item->classes, 1)) {
                $column_count++;
            }
        }
        if ($post_id == $item->menu_item_parent) {
            $li_count++;
            if ($column_count < $column_limit && $li_count == 1) {
                $column_count++;
            }
            if (in_array("break", $item->classes, 1) && $li_count > 1 && $column_count < $column_limit) {
                $column_count++;
            }
            $tmp[$x_key] = $column_count;
        }
    }
    foreach($tmp as $key => $value) {
        $items[$key]->classes[] = sprintf("mega-menu-columns-%d", $value);
    }
    unset($tmp);
    return $items;
};

// add the column classes
function wp_nav_menu_args($args){
    if ($args["walker"] instanceof megaMenuWalker) {
        add_filter("wp_nav_menu_objects", "add_column_number");
    }
    return $args;
};
add_filter("wp_nav_menu_args",'wp_nav_menu_args');



// stop the column classes function

function wp_nav_menu_2($nav_menu){
    remove_filter("wp_nav_menu_objects", "add_column_number");
    return $nav_menu;
};
add_filter("wp_nav_menu",'wp_nav_menu_2');

// /**
//  * Remove the id="" on nav menu items
//  * Return 'menu-slug' for nav menu classes
//  */
function nav_menu_css_class($classes, $item) {
  $slug = sanitize_title($item->title);
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item))/', 'active', $classes); //replaces current-menu-item with active
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(parent))/', 'parent-active', $classes);
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(ancestor))/', '', $classes);
  // $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
  $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

  $classes[] = 'menu-' . $slug;

  $classes = array_unique($classes);

  return array_filter($classes, 'is_element_empty');
}
add_filter('nav_menu_css_class', 'nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', '__return_null');
