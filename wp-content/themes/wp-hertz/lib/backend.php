<?php



// function remove_menus(){
  
//   remove_menu_page( 'index.php' );                  //Dashboard
//   remove_menu_page( 'edit.php' );                   //Posts
//   // remove_menu_page( 'edit.php?post_type=page' );    //Pages
//   remove_menu_page( 'edit-comments.php' );          //Comments
//   // remove_menu_page( 'themes.php' );                 //Appearance
//   // remove_menu_page( 'plugins.php' );                //Plugins
//   // remove_menu_page( 'users.php' );                  //Users
//   // remove_menu_page( 'tools.php' );                  //Tools
//   // remove_menu_page( 'options-general.php' );        //Settings
  
// }
// add_action( 'admin_menu', 'remove_menus' );


// remove unnecessary menus
function remove_admin_menus () {
	global $menu;

	// all users
	$restrict = explode(',', 'Links,Kommentare,Beiträge');
	
	// non-administrator users
	$restrict_user = explode(',', 'Media,Profile,Appearance,Plugins,Users,Tools,Settings');

	// WP localization
	$f = create_function('$v,$i', 'return __($v);');
	array_walk($restrict, $f);
	if (!current_user_can('activate_plugins')) {
		array_walk($restrict_user, $f);
		$restrict = array_merge($restrict, $restrict_user);
	}

	// remove menus
	end($menu);
	while (prev($menu)) {
		$k = key($menu);
		$v = explode(' ', $menu[$k][0]);
		if(in_array(is_null($v[0]) ? '' : $v[0] , $restrict)) unset($menu[$k]);
	}

}
add_action('admin_menu', 'remove_admin_menus');


// remove posts from "new" dropdown
add_action( 'admin_bar_menu', 'remove_posts_from_dropdown', 999 );

function remove_posts_from_dropdown( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'new-post' );
}

// modify tinymce
function mce_mod( $init ) {
    $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4';

    // $style_formats = array (
    //     array( 'title' => 'Bold text', 'inline' => 'b' ),
    //     array( 'title' => 'Red text', 'inline' => 'span', 'styles' => array( 'color' => '#ff0000' ) ),
    //     array( 'title' => 'Red header', 'block' => 'h1', 'styles' => array( 'color' => '#ff0000' ) ),
    //     array( 'title' => 'Example 1', 'inline' => 'span', 'classes' => 'example1' ),
    //     array( 'title' => 'Example 2', 'inline' => 'span', 'classes' => 'example2' )
    // );

    // $init['style_formats'] = json_encode( $style_formats );

    // $init['style_formats_merge'] = false;
    return $init;
}
add_filter('tiny_mce_before_init', 'mce_mod');

function mce_add_buttons( $buttons ){
    array_splice( $buttons, 1, 0, 'styleselect' );
    array_splice( $buttons, 1, 0, 'address' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'mce_add_buttons' );


function remove_mce_buttons_2($buttons) {
	$buttons = array_diff($buttons, array('styleselect', 'alignjustify', 'forecolor', 'backcolor','indent', 'outdent'));
	return $buttons;
}
add_filter('mce_buttons_2', 'remove_mce_buttons_2');