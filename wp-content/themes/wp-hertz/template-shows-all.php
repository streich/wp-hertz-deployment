<?php
/*
Template Name: Shows All
*/
?>

<div class="content content--white content--allshows" >
  <div class="content__inner container ">


<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
<?php endwhile; ?>

    <div class="">

    <?php


    $taxonomy     = 'show';
    $orderby      = 'id';
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no
    $title        = '';

    $args = array(
      'orderby'      => $orderby,
      'show_count'   => $show_count,
      'pad_counts'   => $pad_counts,
      'hierarchical' => $hierarchical,
      'child_of'          => 0,
      'parent'            => 0,
      'hide_empty' => 0
    );

    $terms = get_terms($taxonomy,$args);


    // print_r($categories);
    foreach($terms as $term): ?>

    <div class="row group-category">
    <div class="col-md-2">
    <h2>
     <?php
      echo $term->name;
      ?>
    </h2>
  </div>

    <div class="col-md-10">
     <?php

      $subterms = get_terms($taxonomy, array(
          'parent'   => $term->term_id,
          'hide_empty' => false
          ));


     foreach ( $subterms as $subterm ): ?>

    <?php
      $image = get_field('show_image', $subterm);
      if($image)
        $image_url = $image["sizes"]["medium"];
      else
        $image_url = get_template_directory_uri()."/build/img/show_general.png";


        // $show_description = $cats[0]->description;

      $short_description = $subterm->description;
      if(!$short_description) $short_description = "Noch keine Beschreibung vorhanden. ";
    ?>
      <article class="article-category ">
        <img class="article-category__img" src="<?php echo $image_url; ?>">
        <h3 class="article-category__title"><a href="<?php echo get_term_link(intval($subterm->term_id), $taxonomy);?> "> <?php echo $subterm->name; ?></a></h3>
        <div class="article-category__body"><?php echo $short_description; ?></div>
      </article>

      <?php endforeach; ?>
    </div>
    </div>
    <?php endforeach; ?>



    </div>
</div>
</div>
