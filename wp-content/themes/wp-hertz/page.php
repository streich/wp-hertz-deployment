
<div class="content content--white" >
  <div class="container">

	<section class="section-content row">
  	<?php if( get_field( 'menu' ) ): ?>
	<div class="col-md-3">
  	<?php
        $defaults = array(
			'theme_location'  => 'page_navigation',
			'menu'            => get_field( 'menu' ),
			'container'       => 'nav',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'nav-sub',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
		);

		wp_nav_menu( $defaults );
	?>
  <?php
  if(has_post_thumbnail($post->ID)){
      $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'page-full', false, '' );
      $image = $src[0];         // Medium resolution
    }else{
      $image = get_field('show_image', $cats[0])["url"];
    }

    ?>
	  </div>
	<div class="col-md-9">
<?php else: ?>
	<div class="col-md-12">
<?php endif; ?>

    <div class="breadcrumbs">
        <?php the_breadcrumb(); ?>
  	</div>



		  <h1 class="page_title">
		    <?php echo the_title(); ?>
		  </h1>

		   <?php
			$page_description = get_field('subtitle');
			if($page_description)
			{
				echo '<p class="page_description lead">'.$page_description.'</p>';
			}
		?>

    <p>
      <img src="<?php echo $image;?>" alt="" >
      <?php get_template_part('templates/element', 'cc'); ?>
    </p>


	      <?php echo wpautop($post->post_content); ?>

 <?php
	$image = get_field('page_image');
	if($image)
	{
  		$image_url = $image["sizes"]["medium"];
		echo '<img class="lead" src="'.$image.'"/>';
	}

?>

 <?php
	$lead = get_field('page_lead',false,false);
	if($lead)
	{
		echo '<p>'.$lead.'</p>';
	}

?>





<?php while(the_flexible_field("content")): ?>

 	<?php if(get_row_layout() == "lead"): // layout: Content ?>



		 <?php
			$lead = get_sub_field('text',false,false);
			if($lead)
			{
				echo '<p class="lead">'.$lead.'</p>';
			}

		?>



 	<?php elseif(get_row_layout() == "text"): // layout: Content ?>



		 <?php
			$text = get_sub_field('text');
			if($text)
			{
				echo $text;
			}
		?>




	<?php elseif(get_row_layout() == "link_list"): ?>
	 <?php
		$rows = get_sub_field('link_list');
		if($rows)
		{
			echo '<ul class="linklist">';
			foreach($rows as $row){
				echo '<li><a target="_blank" href="'.$row['link'].'">' .$row['name'] .'</a> <p class="caption">' .$row['description'] .'</p></li>';
			}

			echo '</ul>';
		}

		?>
	<?php elseif(get_row_layout() == "image_with_cc"): // layout: Content ?>
	  <?php
		$image = get_sub_field('image');

		if($image)
		{
	  		$image_url = $image["sizes"]["large"];
			echo '<img class="lead" src="'.$image_url.'"/>';
		}

		$cc = get_sub_field('cc');
		if($cc)
		{
			echo '<p class="caption">'.$cc.'"</p>';
		}

	?>


	<?php elseif(get_row_layout() == "2col"): // layout: File ?>

	<?php the_sub_field("col1"); ?>

		<?php endif; ?>

	<?php endwhile; ?>


	 <?php

	$rows = get_field('links');
	if($rows)
	{
		echo '<div class="row clearfix"><h4>Weiterf«ährende Links</h4><ul class="no-style">';

		foreach($rows as $row)
		{
			echo '<li><a target="_blank" href="'.$row['link'].'">' .$row['name'] .'</a> <p class="caption">' .$row['description'] .'</p></li>';
		}

		echo '</ul></div>';
	}

?>

</div>
</section>
</div>
</div>
