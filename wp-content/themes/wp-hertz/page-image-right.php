<?php
/*
Template Name: Page Image Right
*/
?>
<?php
if(has_post_thumbnail($post->ID)){
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 400,550 ), false, '' );
    $image = $src[0];         // Medium resolution
  }else{
    $image = get_field('show_image', $cats[0])["url"];
  }

  ?>

<div class="content" >
  <div class="container">
    <section class="section-content row">

    	<div class="col-sm-6">
    		  <div class="breadcrumbs">
            <?php the_breadcrumb(); ?>
          </div>
    		  <h1>
    		    <?php echo the_title(); ?>
    		  </h1>

    		   <?php
    			$page_description = get_field('subtitle');
    			if($page_description)
    			{
    				echo '<p class="page_description">'.$page_description.'</p>';
    			}
    		?>

	      <?php echo wpautop($post->post_content); ?>
    	</div>

      <div class="col-sm-6">
        <img src="<?php echo $image;?>" alt="" style="width: 100%; margin-top: 60px">
        <?php get_template_part('templates/element', 'cc'); ?>
      </div>

    </section>
	</div>
</div>
