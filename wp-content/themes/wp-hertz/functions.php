<?php

require_once 'lib/backend.php';
require_once 'lib/acf-config.php';

require_once 'lib/posttypes.php';
require_once 'lib/widgets.php';

require_once 'lib/utils.php';
require_once 'lib/wrapper.php';
require_once 'lib/nav.php';
require_once 'lib/sidebars.php';
require_once 'lib/scripts.php';


/**
 * Configuration values
 */
define('GOOGLE_ANALYTICS_ID', ''); // UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)
  define('WP_ENV', 'development');

if (!defined('WP_ENV')) {
  define('WP_ENV', 'production');  // scripts.php checks for values 'production' or 'development'

}
define('WP_DEBUG', true);

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

/**
 * initial setup and constants
 */
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/roots-translations
  // load_theme_textdomain('roots', get_template_directory() . '/lang');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus(array(
    'primary_navigation' => 'Primary Navigation'
  ));

  register_nav_menus(array(
    'header_navigation' => 'Header Navigation'
  ));

  register_nav_menus(array(
    'page_navigation' => 'Page Navigation'
  ));

  register_nav_menus(array(
    'people_navigation' => 'People Navigation'
  ));

  register_nav_menus(array(
    'current_track_navigation' => 'Current Track Navigation'
  ));


  // Add post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  // add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'));

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', array('caption', 'comment-form', 'comment-list'));

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style('/assets/css/editor-style.css');
}

add_action('after_setup_theme', 'setup');


// AUTO P OR NOT ?
//
// remove_filter( 'the_content', 'wpautop' );
// remove_filter( 'the_excerpt', 'wpautop' );


// add search box to menu
add_filter('wp_nav_menu_items','add_search_box_to_menu', 10, 2);
function add_search_box_to_menu( $items, $args ) {
    if( $args->theme_location == 'primary_navigation' )
    return $items."<li> <button class='button-toggle-search js-toggle-search' data-toggle='collapse' data-target='.head-search'><svg class='icon-24'>  <use xlink:href='".theme_root()."/build/svg/icons.svg#icon-search'></use></svg></button></li>";


    return $items;
}

// returns root folder

function theme_root(){
  return get_template_directory_uri();
}

// breadcrumbs
function the_breadcrumb_b() {
  if (!is_home()) {
    echo '<a href="';
    echo get_option('home');
    echo '">';
    bloginfo('name');
    echo "</a> » ";
    if (is_category() || is_single()) {
      the_category('title_li=');
      if (is_single()) {
        echo " » ";
        the_title();
      }
    } elseif (is_page()) {
      echo the_title();
    }
  }
}


/**
 * Klasse zum Erstellen einer WordPress Breadcrumb Navigation.
 *
 * @author Dominik Schilling
 * @license GPLv2
 * @link http://wpgrafie.de/204/
 *
 * @version 0.1.1
 */
class DS_WP_Breadcrumb {
  /**
   * The list of breadcrumb items.
   *
   * @var array
   * @since 1.0.0
   */
  public $breadcrumb;

  /**
   * Templates for link, current/standard state and before/after.
   *
   * @var array
   */
  public $templates;

  /**
   * Various strings.
   *
   * @var array
   */
  public $strings;

  /**
   * Various options.
   *
   * @var array
   * @access public
   */
  public $options;

  /**
   * Constructor.
   *
   * @param array $templates An array with templates for link, current/standard state and before/after.
   * @param array $options An array with options.
   * @param array $strings An array with strings.
   * @param bool $autorun Autorun or not.
   * @return string
   */
  public function __construct( $templates = array(), $options = array(), $strings = array(), $autorun = true ) {
    $this->templates = wp_parse_args(
      $templates,
      array(
        'link' => '<a href="%s">%s</a>',
        'current' => '<span class="c">%s</span>',
        'standard' => '<span class="s">%s</span>',
        'before' => '<nav>',
        'after' => '</nav>'
      )
    );
    $this->options = wp_parse_args( $options, array(
      'separator' => ' â€º ',
      'posts_on_front' => 'posts' == get_option( 'show_on_front' ) ? true : false,
      'page_for_posts' => get_option( 'page_for_posts' ),
      'show_pagenum' => true, // support pagination
      'show_htfpt' => false // show hierarchical terms for post types
    ) );
    $this->strings = wp_parse_args( $strings, array(
      'home' => 'Startseite',
      'search' => array(
        'singular' => 'Ein Suchergebnis zu <em>%s</em>',
        'plural'   => '%s Suchergebnisse zu <em>%s</em>'
      ),
      'paged' => 'Seite %d',
      '404_error' => 'Fehler: Seite existiert nicht'
    ) );

    // Generate breadcrumb
    if ( $autorun)
      echo $this->output();
  }

  /**
   * Return the final breadcrumb.
   *
   * @return string
   */
  public function output() {
    if ( empty( $this->breadcrumb ) )
      $this->generate();

    $breadcrumb = (string) implode( $this->options['separator'], $this->breadcrumb );

    return $this->templates['before'] . $breadcrumb . $this->templates['after'];
  }

  /**
   * Build the item based on the type.
   *
   * @param string|array $item
   * @param string $type
   * @return string
   */
  protected function template( $item, $type = 'standard' ) {
    if ( is_array( $item ) )
      $type = 'link';

    switch ( $type ) {
      case'link':
        return $this->template(
          sprintf(
            $this->templates['link'],
            esc_url( $item['link'] ),
            $item['title']
          )
        );
        break;
      case 'current':
        return sprintf( $this->templates['current'], $item );
        break;
      case 'standard':
        return sprintf( $this->templates['standard'], $item );
        break;
    }
  }

  /**
   * Helper to generate taxonomy parents.
   *
   * @param mixed $term_id
   * @param mixed $taxonomy
   * @return void
   */
  protected function generate_tax_parents( $term_id, $taxonomy ) {
    $parent_ids = array_reverse( get_ancestors( $term_id, $taxonomy ) );

    foreach ( $parent_ids as $parent_id ) {
      $term = get_term( $parent_id, $taxonomy );
      $this->breadcrumb["archive_{$taxonomy}_{$parent_id}"] = $this->template( array(
        'link' => get_term_link( $term->slug, $taxonomy ),
        'title' => $term->name
      ) );
    }
  }

  /**
   * Generate the breadcrumb.
   *
   * @return void
   */
  public function generate() {
    $post_type = get_post_type();
    $queried_object = get_queried_object();
    $this->options['show_pagenum'] = ( $this->options['show_pagenum'] && is_paged() ) ? true : false;


    // Home & Front Page
    $this->breadcrumb['home'] = $this->template( $this->strings['home'], 'current' );
    $home_linked = $this->template( array(
      'link' => home_url( '/' ),
      'title' => $this->strings['home']
    ) );


    if ( $this->options['posts_on_front'] ) {
      if ( ! is_home() || $this->options['show_pagenum'] )
        $this->breadcrumb['home'] = $home_linked;
    } else {
      if ( ! is_front_page() )
        $this->breadcrumb['home'] = $home_linked;

      if ( is_home() && !$this->options['show_pagenum'] )
        $this->breadcrumb['blog'] = $this->template( get_the_title( $this->options['page_for_posts'] ), 'current' );

      if ( ( 'post' == $post_type && ! is_search() && ! is_home() ) || ( 'post' == $post_type && $this->options['show_pagenum'] ) )
        $this->breadcrumb['blog'] = $this->template( array(
          'link' => get_permalink( $this->options['page_for_posts'] ),
          'title' => get_the_title( $this->options['page_for_posts'] )
        ) );
    }

    // Post Type Archive as index
    if ( is_singular() || ( is_archive() && ! is_post_type_archive() ) || is_search() || $this->options['show_pagenum'] ) {
      if ( $post_type_link = get_post_type_archive_link( $post_type ) ) {
        $post_type_label = get_post_type_object( $post_type )->labels->name;
        $this->breadcrumb["archive_{$post_type}"] = $this->template(
          array(
          'link' => $post_type_link,
          'title' => $post_type_label
        ) );
      }
    }

    if ( is_singular() ) { // Posts, (Sub)Pages, Attachments and Custom Post Types
      if ( ! is_front_page() ) {
        if ( $this->options['show_htfpt'] ) {
          $_id = $queried_object->ID;
          $_post_type = $post_type;

          if ( is_attachment() ) {
            // Show terms of the parent page
            $_id = $queried_object->post_parent;
            $_post_type = get_post_type( $_id );
          }

          $taxonomies = get_object_taxonomies( $_post_type, 'objects' );
          $taxonomies = array_values( wp_list_filter( $taxonomies, array(
            'hierarchical' => true
          ) ) );

          if ( ! empty( $taxonomies ) ) {
            $taxonomy = $taxonomies[0]->name; // Get the first taxonomy
            $terms = get_the_terms( $_id, $taxonomy );

            if ( ! empty( $terms ) ) {
              $terms = array_values( $terms );
              $term = $terms[0]; // Get the first term

              if ( 0 != $term->parent )
                $this->generate_tax_parents( $term->term_id, $taxonomy );

              $this->breadcrumb["archive_{$taxonomy}"] = $this->template( array(
                'link' => get_term_link( $term->slug, $taxonomy ),
                'title' => $term->name
              ) );
            }
          }
        }

        if ( 0 != $queried_object->post_parent ) { // Get Parents
          $parents = array_reverse( get_post_ancestors( $queried_object->ID ) );

          foreach ( $parents as $parent ) {
            $this->breadcrumb["archive_{$post_type}_{$parent}"] = $this->template( array(
              'link' => get_permalink( $parent ),
              'title' => get_the_title( $parent )
            ) );
          }
        }


        $this->breadcrumb["single_{$post_type}"] = $this->template( get_the_title(), 'current' );
      }
    } elseif ( is_search() ) { // Search
      $total = $GLOBALS['wp_query']->found_posts;
      $text = sprintf(
        _n(
          $this->strings['search']['singular'],
          $this->strings['search']['plural'],
          $total
        ),
        $total,
        get_search_query()
      );

      $this->breadcrumb['search'] = $this->template( $text, 'current' );

      if ( $this->options['show_pagenum'] )
        $this->breadcrumb['search'] = $this->template( array(
          'link' => home_url( '?s=' . urlencode( get_search_query( false ) ) ),
          'title' => $text
        ) );
    } elseif ( is_archive() ) { // All archive pages
      if ( is_category() || is_tag() || is_tax() ) { // Categories, Tags and Custom Taxonomies
        $taxonomy = $queried_object->taxonomy;

        if ( 0 != $queried_object->parent && is_taxonomy_hierarchical( $taxonomy ) ) // Get Parents
          $this->generate_tax_parents( $queried_object->term_id, $taxonomy );

        $this->breadcrumb["archive_{$taxonomy}"] = $this->template( $queried_object->name, 'current' );

        if ( $this->options['show_pagenum'] )
          $this->breadcrumb["archive_{$taxonomy}"] = $this->template( array(
            'link' => get_term_link( $queried_object->slug, $taxonomy ),
            'title' => $queried_object->name
          ) );

      } elseif ( is_date() ) { // Date archive
        if ( is_year() ) { // Year archive
          $this->breadcrumb['archive_year'] = $this->template( get_the_date( 'Y' ), 'current' );

          if ( $this->options['show_pagenum'] )
            $this->breadcrumb['archive_year'] = $this->template( array(
              'link' => get_year_link( get_query_var( 'year' ) ),
              'title' => get_the_date( 'Y' )
            ) );
        } elseif ( is_month() ) { // Month archive
          $this->breadcrumb['archive_year'] = $this->template( array(
            'link' => get_year_link( get_query_var( 'year' ) ),
            'title' => get_the_date( 'Y' )
          ) );
          $this->breadcrumb['archive_month'] = $this->template( get_the_date( 'F' ), 'current' );

          if ( $this->options['show_pagenum'] )
            $this->breadcrumb['archive_month'] = $this->template( array(
              'link' => get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) ),
              'title' => get_the_date( 'F' )
            ) );
        } elseif ( is_day() ) { // Day archive
          $this->breadcrumb['archive_year'] = $this->template( array(
            'link' => get_year_link( get_query_var( 'year' ) ),
            'title' => get_the_date( 'Y' )
          ) );
          $this->breadcrumb['archive_month'] = $this->template( array(
            'link' => get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) ),
            'title' => get_the_date( 'F' )
          ) );
          $this->breadcrumb['archive_day'] = $this->template( get_the_date( 'j' ) );

          if ( $this->options['show_pagenum'] )
            $this->breadcrumb['archive_day'] = $this->template( array(
              'link' => get_month_link(
                get_query_var( 'year' ),
                get_query_var( 'monthnum' ),
                get_query_var( 'day' )
              ),
              'title' => get_the_date( 'F' )
            ) );
        }
      } elseif ( is_post_type_archive() && ! is_paged() ) { // Custom Post Type Archive
        $post_type_label = get_post_type_object( $post_type )->labels->name;
        $this->breadcrumb["archive_{$post_type}"] = $this->template( $post_type_label, 'current' );
      } elseif ( is_author() ) { // Author archive
        $this->breadcrumb['archive_author'] = $this->template( $queried_object->display_name, 'current' );
      }
    } elseif ( is_404() ) {
      $this->breadcrumb['404'] = $this->template( $this->strings['404_error'], 'current' );
    }

    if ( $this->options['show_pagenum'] )
      $this->breadcrumb['paged'] = $this->template(
        sprintf(
          $this->strings['paged'],
          get_query_var( 'paged' )
        ),
        'current'
      );
  }
}

function ds_breadcrumb() {
  // require_once( TEMPLATEPATH . '/inc/class-wordpress-breadcrumb.php' );

  $templates = array(
    'before' => '',
    'after' => '',
    'standard' => '%s',
    'current' => '<span class="current">%s</span>',
    'link' => '<a href="%s" itemprop="url"><span itemprop="title">%s</span></a>'
  );
  $options = array(
    'separator' => '<span>/</span>',
    'show_htfpt' => true
  ); $strings = array(
    'home' => 'Hertz 87.9'
  );

  $breadcrumb = new DS_WP_Breadcrumb( $templates, $options, $strings );
}

function the_breadcrumb() {
  ds_breadcrumb();
}


function init_images() {
  add_theme_support('post-thumbnails');
  add_image_size( 'page-full', 1140, 400); // 1009 pixels wide (and unlimited height)
  add_image_size( 'article-full', 780, 300 ); // 300 pixels wide (and unlimited height)
  add_image_size( 'article-preview', 360, 180, true ); // not used (cropped)
  add_image_size( 'people-full', 460 ); // (cropped)
  add_image_size( 'people-preview', 270, 270, true ); // (cropped)
}
add_action( 'after_setup_theme', 'init_images' );

function my_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'article-full' => 'Artikel (Vorschau)' ,
        'article-preview' => '' , // nicht mehr benutzt
        'page-full' => 'Artikel (volle Breite)' ,
        'people-full' => 'Menschen' ,
        'people-preview' => 'Menschen (Vorschau)'
    ) );
}
add_filter( 'image_size_names_choose', 'my_custom_sizes' );



function filter_ptags_on_images($content)
{
    // do a regular expression replace...
    // find all p tags that have just
    // <p>maybe some white space<img all stuff up to /> then maybe whitespace </p>
    // replace it with just the image tag...
    return preg_replace('/<p>(\s*)(<img .* \/>)(\s*)<\/p>/iU', '\2', $content);
}

// we want it to be run after the autop stuff... 10 is default.
add_filter('the_content', 'filter_ptags_on_images');



/**
 * Add Photographer Name and URL fields to media uploader
 *
 * @param $form_fields array, fields to include in attachment form
 * @param $post object, attachment record in database
 * @return $form_fields, modified form fields
 */

function be_attachment_field_credit( $form_fields, $post ) {
	$form_fields['be-photographer-name'] = array(
		'label' => 'Urheber Name',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_name', true ),
		// 'helps' => 'If provided, photo credit will be displayed',
	);

	$form_fields['be-photographer-url'] = array(
		'label' => 'Urheber URL',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_photographer_url', true ),
		// 'helps' => 'Add Photographer URL',
	);

  $form_fields['be-licence-name'] = array(
    'label' => 'Lizenz Name',
    'input' => 'text',
    'value' => get_post_meta( $post->ID, 'be_licence_name', true ),
    // 'helps' => 'If provided, photo credit will be displayed',
  );

	$form_fields['be-licence-url'] = array(
		'label' => 'Lizenz URL',
		'input' => 'text',
		'value' => get_post_meta( $post->ID, 'be_licence_url', true ),
		// 'helps' => 'Add Photographer URL',
	);

	return $form_fields;
}

add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );

/**
 * Save values of Photographer Name and URL in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */

function be_attachment_field_credit_save( $post, $attachment ) {
	if( isset( $attachment['be-photographer-name'] ) )
		update_post_meta( $post['ID'], 'be_photographer_name', $attachment['be-photographer-name'] );

	if( isset( $attachment['be-photographer-url'] ) )
update_post_meta( $post['ID'], 'be_photographer_url', esc_url( $attachment['be-photographer-url'] ) );

	if( isset( $attachment['be-licence-name'] ) )
		update_post_meta( $post['ID'], 'be_licence_name', $attachment['be-licence-name'] );

	if( isset( $attachment['be-licence-url'] ) )
update_post_meta( $post['ID'], 'be_licence_url', esc_url( $attachment['be-licence-url'] ) );

	return $post;
}

add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );


add_filter( 'wp_audio_shortcode', 'audio_short_fix', 10, 5 );
function audio_short_fix( $html, $atts, $audio, $post_id, $library )
{
$html = str_replace ( 'visibility: hidden;' ,'' , $html );
return $html;
}
?>
