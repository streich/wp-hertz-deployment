<?php
/*
Template Name: Schedule
*/
?>

<div class="content">
  <div class="container container-stretch">

    <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php endwhile; ?>


    <div class="row">

      <?php

$taxonomy     = 'show';
$orderby      = 'name';
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';

$args = array(
  'orderby'      => $orderby,
  'show_count'   => $show_count,
  'pad_counts'   => $pad_counts,
  'hierarchical' => $hierarchical,
  'hide_empty' => 0
);

$terms = get_terms($taxonomy,$args);

$days_map = array(
  "Montag" => 0,
  "Dienstag" => 1,
  "Mittwoch" => 2,
  "Donnerstag" => 3,
  "Freitag" => 4,
  "Samstag" => 5,
  "Sonntag" => 6
  );

$days_array = array(
  "Montag",
  "Dienstag",
  "Mittwoch",
  "Donnerstag",
  "Freitag",
  "Samstag",
  "Sonntag"
  );

$complete_schedule = getScheduleArray();


  // print_r($complete_schedule)
function getScheduleArray(){
  $a = array();
  for( $hour = 6; $hour < 26; $hour++){
      $a[$hour] = array(array(),array(),array(),array(),array(),array(),array());
  }
  return $a;

}

function getFieldFromTime($hour,$time){

}

// print_r($categories);
foreach($terms as $term){
  $schedule = get_field('schedule', $term);
  // print_r($term);
  if( have_rows('schedule',$term) ):
    // loop through the rows of data
    while ( have_rows('schedule',$term) ) : the_row($term);
  // [$days_map[get_sub_field('day')]
      // add new event to time
      $hour_start = get_sub_field('hour');
      $hour_end = get_sub_field('hour_end');
      if($hour_start < 3){
        $hour_start += 24;
      }

      if($hour_end < 3){
        $hour_end += 24;
      }

      // if($hour_end < 3 && $hour_end < $hour_start){
      //    $hour_end = $hour_end + 24 ;
      // }
      // if($hour_start == 0 && $hour_end < 3){
      //    $hour_start = 24;
      //    $hour_end = $hour_end + 24 ;
      // }
      //   $eventEndHour = $eventEndHour + $eventStartHour;
      // }

        $event = array(
          "name"=>$term->name,
          "link"=> get_term_link($term),
          "hour" => $hour_start,
          "color" => get_field("color",$term),
          "schedule_type" => get_field("schedule_type",$term),
          "startHour"=>$hour_start,
          "startMinute"=> sprintf("%02d", get_sub_field('minute')),
          "endHour"=>  $hour_end,
          "endMinute"=>sprintf("%02d", get_sub_field('minute_end')),
          "timeunits"=> $hour_end - $hour_start
        );

      // if(!empty($complete_schedule[get_sub_field('hour')][$days_map[get_sub_field('day')]])){
      //   $test = true;
      // }

      // prevents error when there is a program before 6
      if($complete_schedule[$hour_start])
        array_push($complete_schedule[$hour_start][$days_map[get_sub_field('day')]],$event);
       // print_r($complete_schedule[get_sub_field('hour')][$days_map[get_sub_field('day')]]);
       // echo "<hr>";
    endwhile;
  endif;

}
  // prepprocess schedule to concat events that are overlapping
  //
  foreach($complete_schedule as $i => $hours){
    foreach($hours as $j => $day){
      foreach($day as $e => $event){
          $timeunits  = $event["timeunits"];
          for($k = $i; $k < ($i+$timeunits); $k++){

              if(!empty($complete_schedule[$k][$j])){
                 foreach($complete_schedule[$k][$j] as $eB => $eventB){
                    array_push($complete_schedule[$i][$j],$complete_schedule[$k][$j][$eB]);
                    unset($complete_schedule[$k][$j][$eB]);
                }
              }else{
                    // remove if being overlapped

                    $complete_schedule[$k][$j] = false;
              }
          }

      }
    }
  }


function getMaxTimeunits($day){
  $max = 0;
  $hourMin = 26;
  $hourMax = 0;
  foreach($day as $event){
      $eventEndHour = $event["endHour"];
      $eventStartHour = $event["startHour"];

      if($eventEndHour > $hourMax){
        $hourMax = $eventEndHour;
      }

      if($eventStartHour < $hourMin){
        $hourMin = $eventStartHour;
      }
      $max = $hourMax - $hourMin;
    }
    if($max != 0)
      return $max;
    else
      return false;
  }
?>


    </div>
    <div class="table-responsive">
      <table class="table-timetable">
        <thead>
          <tr>
            <th></th>
            <th>Montag</th>
            <th>Dienstag</th>
            <th>Mittwoch</th>
            <th>Donnerstag</th>
            <th>Freitag</th>
            <th>Samstag</th>
            <th>Sonntag</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($complete_schedule as $key => $hour): ?>
          <tr>
            <td class="td-hours">
              <?php echo $key%24; ?>:00 Uhr
            </td>
            <?php foreach($hour as $key => $day): ?>

            <?php if(is_array($day)): ?>

            <td class="<?php echo empty($day)? "empty": "td-event"; echo " items_".count($day); ?>"
              rowspan="<?php echo getMaxTimeunits($day);?>" style="background-color: <?php echo $day[0]["color"]; ?>">
              <?php foreach($day as $key => $event): ?>
              <a href="<?php echo $event["link"]; ?>"
                class="event_container event_container--color-<?php echo $event["color"]; ?>">
                <h4 class="event_title" title="<?php echo $event["name"]; ?>"><?php echo $event["name"]; ?></h4>
                <?php if($event["schedule_type"]): ?>
                <?php if($event["schedule_type"] == 1): ?>
                <span> zweiwöchentlich </span>
                <?php elseif($event["schedule_type"] ==  2): ?>
                <!-- <span> unregelmäßig </span> -->
                <?php elseif($event["schedule_type"] ==  3): ?>
                <span> einmal im Monat </span>
                <?php endif; ?>
                <?php endif; ?>
                <div class="top_hour">
                  <span class="hours"> <?php echo ($event["startHour"]%24).":".$event["startMinute"]; ?></span> -
                  <span class="hours"><?php echo ($event["endHour"]%24).":".$event["endMinute"]; ?></span>
                </div>
              </a>
              <?php endforeach; ?>
            </td>
            <?php endif; ?>
            <?php endforeach; ?>
          </tr>
          <?php endforeach; ?>


        </tbody>
      </table>
    </div>
  </div>
</div>