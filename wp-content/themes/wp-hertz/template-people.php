<?php
/*
Template Name: People
*/
?>


<div class="content content--white">
  <div class="container">

    <section class="section-content row">
      <?php if( get_field( 'menu' ) ): ?>
      <div class="col-md-3">
        <?php
          $defaults = array(
  			'theme_location'  => '',
  			'menu'            => get_field( 'menu' ),
  			'container'       => 'nav',
  			'container_class' => '',
  			'container_id'    => '',
  			'menu_class'      => 'nav-sub',
  			'menu_id'         => '',
  			'echo'            => true,
  			'fallback_cb'     => 'wp_page_menu',
  			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  			'depth'           => 0,
  			'walker'          => ''
  		);

  		wp_nav_menu( $defaults );
  	?>
        <?php
    if(has_post_thumbnail($post->ID)){
        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 400,550 ), false, '' );
        $image = $src[0];         // Medium resolution
      }else{
        $image = get_field('show_image', $cats[0])["url"];
      }

      ?>
      </div>
      <div class="col-md-9">
        <?php else: ?>
        <div class="col-md-12">
          <?php endif; ?>
          <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/page', 'header'); ?>
          <?php endwhile; ?>
          <div class="row">
            <?php
	//WordPress loop for custom post type
	 $my_query = new WP_Query('post_type=people&posts_per_page=-1&orderby=title&order=ASC');
	  while ($my_query->have_posts()):

	   $my_query->the_post();
		if(has_post_thumbnail($post->ID)){
	              $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), "people-preview", false, '' );
	              $image = $src[0];         // Medium resolution
	           }else{
	           	$image = false;
	           }
	  ?>
            <article class="article-people col-md-4  col-sm-4 col-xs-12 js-match-height">
              <header class="article-people__header">
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                  <div class="article-people__image-container">

                    <img class="article-people__image" src="<?php echo $image; ?>">
                  </div>
                </a>
              </header>
              <div class="article-people__body">
                <h3 class="article-people__title">
                  <b> <?php single_cat_title( '', true );  ?> </b>
                  <?php the_title(); ?></h3>
              </div>
            </article>
            <?php endwhile;  wp_reset_query(); ?>

          </div>
        </div>
      </div>
  </div>
</div>