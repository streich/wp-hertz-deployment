DOCUMENTATION.md

# Taxonomies/Custom Categories




# Advanced Custom Fields

# Sass BEM NAMING

```sass

	.header
	.footer
	
	 
	.navbar-top
	.navbar-main

	.nav-main
	.nav-top

	.content-header
		.content-header__inner
	.content-header--fullwidth

	.content 
		.content__inner
	.content--start 
	.content--allshows 

	.article-category
		.article-category__image
		.article-category__title


	.article-news

	.article-event

	.article-default

	.article-show
		.article-show__footer
		.article-show__header
		.article-show__title

	.article-show--compact

	// events
	.article-event-slide
		.article-event-slide__inner

	.article-event-slide--textonly
	.article-event-slide--both
	.article-event-slide--immageonly

```

# Getting a field value from a term

```php
...

$terms = get_terms($taxonomy,$args);

foreach($terms as $term): ?>
<?php
  $image = get_field('show_image', $term);
  $image_url = $image["sizes"]["medium"];
?>
    <img class="show-category-image" src="<?php echo $image_url; ?>">

<?php endforeach; ?>

...

```


# Setup auto deployment

http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

# Lorem Ipsum

Lorem ipsum Excepteur in reprehenderit culpa commodo ex culpa eiusmod mollit velit Ut.

Lorem ipsum Ex anim sit labore in sint ut fugiat commodo occaecat laborum minim nostrud exercitation irure Excepteur voluptate commodo officia eiusmod commodo minim in sint aute consequat ut et dolor culpa adipisicing.


Lorem ipsum Non irure sit magna culpa est incididunt exercitation voluptate sunt sint quis occaecat officia Excepteur Duis ad sint elit labore voluptate Excepteur eiusmod sed occaecat adipisicing laboris nostrud ea proident consectetur cupidatat esse ut amet nisi pariatur laborum in fugiat pariatur in esse culpa in ea ullamco commodo quis mollit dolor et ad dolore in in magna sunt elit aliqua proident irure mollit aliqua nulla et sint veniam irure culpa non magna exercitation culpa voluptate consequat Duis cupidatat ea sed enim quis dolor nulla pariatur proident sunt deserunt incididunt proident Duis dolore aute eiusmod labore ut enim irure minim anim fugiat in magna Ut in consequat id id non irure tempor mollit eu ea id Ut commodo cupidatat ad laborum id cillum irure sed Duis dolore consectetur laborum dolor ex sit dolor aliquip magna culpa tempor in Ut officia elit Ut dolor proident quis Excepteur minim occaecat sit dolore deserunt ex voluptate aliqua ut mollit officia minim occaecat ad commodo cupidatat magna elit magna et elit Excepteur pariatur nisi elit enim laborum ut sint sint est consectetur nisi qui id in eiusmod laborum fugiat aliquip et quis deserunt nostrud ut culpa eiusmod in fugiat sint dolore ut Excepteur ut amet sed mollit occaecat irure aliquip amet est do amet labore velit dolor in laborum ut sint Excepteur ea esse dolore laborum commodo elit incididunt id dolore.

Lorem ipsum Velit cupidatat laborum voluptate eiusmod.