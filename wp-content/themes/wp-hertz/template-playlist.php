<?php
/*
Template Name: Playlist
*/
?>

<div class="page-header">
  <div class="container" role="document">
 <div class="breadcrumbs">
	<?php the_breadcrumb(); ?>
	</div>

	<h1 class="pull-left"> Playlist <span class="js-headline-date"> <?php echo current_time( 'd M Y ' ); ?></span></h1> 
	<select class="js-playlist-date-select pull-right hidden" style="margin:20px 0; min-width: 200px">
	</select>


		<div class="btn-group pull-right" role="group" aria-label="...">
  <button type="button" class="btn btn-default js-playlist-date-set-today">Heute</button>
  <button type="button" class="btn btn-default js-playlist-date-set-yesterday">Gestern</button>

  <div class="btn-group js-playlist-date-show-datepicker " role="group">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      Anderes Datum
      <span class="caret"></span>
    </button>
	<input type="text" type="text" class="js-playlist-date-input form-control pull-right " style="width:1px; padding:0; opacity: 0">
   
  </div>
</div>
</div>
</div>

<div class="content" >
<div class="section-playlist-container">
	<section class="section-playlist js-playlist-target">
	</section>
</div>
</div>