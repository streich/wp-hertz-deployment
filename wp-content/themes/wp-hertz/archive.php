<?php

/*
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php

$context = Timber::get_context();
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$queried_object = get_queried_object();
$is_show = $queried_object->taxonomy == 'show';

$args = NULL;
$name = $queried_object->name;
if($is_show){
  $icon_url = NULL;
  $name = $queried_object->taxonomy;
  $category_id = $queried_object->term_id;
  $has_category = isset($category_id);
  if ($has_category) {
    $image_object = get_field('show_image', $queried_object);
    $icon_url = $image_object 
                ? $image_object["url"]
                : get_template_directory_uri() . "/build/img/show_general.png";
  } else {
    $icon_url = get_template_directory_uri() . "/build/img/show_general.png";
  }
  $context['icon'] = $icon_url;
  $context['has_category'] = $has_category;
  $context['has_icon'] = !!$icon_url;
  $context['has_schedule'] = have_rows('schedule', $queried_object);
  $context['schedule'] = get_field('schedule', $queried_object);
  $context['category_title'] = $queried_object->name; // single_cat_title('', true);
  $context['category_slug'] = $queried_object->slug; // single_cat_title('', true);
  $context['category_description'] = get_field('short_description', $queried_object);
  $context['link_program'] = get_permalink(get_page_by_title('Was läuft wann'));
  
  $args = $has_category 
    ? array('post_type' => 'shows', 'orderby' => 'date', 'order'   => 'DESC','tax_query' => array(array('taxonomy' => 'show', 'terms' => $category_id,),), 'pagination' => true,'posts_per_page' => 12, 'paged' => $paged)
    : array('post_type' => 'shows', 'orderby' => 'date', 'order'   => 'DESC','pagination' => true, 'posts_per_page' => 12, 'paged' => $paged);

} else {
  $args = array('post_type' => $queried_object->name, 'pagination' => true,'posts_per_page' => 12, 'paged' => $paged);
  $context['name'] =  $queried_object->name;
  $context['singular_name'] =  $queried_object->labels->singular_name;
  $context['label'] =  $queried_object->labels->name;
  $context['pagination'] = Timber::get_pagination();
  
}
$posts = Timber::get_posts( $args );
$context['posts'] = $posts;
$context['has_posts'] = have_posts();
Timber::render( array( 'archive-' . $name . '.twig', 'archive-' . $name . '.twig', 'archive.twig' ), $context );
?>