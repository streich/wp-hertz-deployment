<?php
/*
Template Name: Start
*/
?>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12 slick-slider__container">
        <!-- <h5 class="section-headline"> Meldungen </h5> -->
        <?php get_template_part( 'templates/module', 'message-slider' ); ?>
      </div>
    </div>
  </div>
</div>
<div class="content" style="background: #fff; padding-top: 200px; margin-top: -180px">
  <div class="container ">

    <div class="row">
      <div class="col-md-7">

        <section class="section-shows-start">
          <h5 class="section-headline"> Artikel auf Hertz 87.9 </h5>
          <?php get_template_part( 'templates/module', 'shows-compact' ); ?>
        </section>
      </div>
      <div class="col-md-5">
        <div class="row">
          <section class="col-md-12 section-event-slider hidden ">
            <h5 class="section-headline"> Veranstaltungstipps </h5>
            <?php get_template_part( 'templates/module', 'events-slider' ); ?>
          </section>
          <section class="col-md-12 section-news-events-tabs">
            <div class="js-tabpanel tab-panel" role="tabpanel">
              <div class="tab-panel__tablist" role="tablist">
                <h5 role="presentation" class="active section-headline tab-panel__tab">
                  <a href="#news" aria-controls="news" role="tab" data-toggle="tab">Nachrichten</a>
                </h5>
                <h5 role="presentation" class="section-headline tab-panel__tab ">
                  <a href="#events" aria-controls="events" role="tab" data-toggle="tab">Veranstaltungen</a>
                </h5>
                <h5 role="presentation" class="section-headline tab-panel__tab ">
                  <a href="#links" aria-controls="links" role="tab" data-toggle="tab">Links</a>
                </h5>
              </div>
              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="news">
                  <?php get_template_part( 'templates/module', 'news-compact' ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="events">
                  <?php get_template_part( 'templates/module', 'events-compact' ); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="links">
                  <?php get_template_part( 'templates/module', 'links-compact' ); ?>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content hidden">
  <div class="container">
    <hr>
    <section class="section-podcasts-start ">
      <h5 class="section-headline"> Podcasts </h5>
      <div class="row">
        <?php get_template_part( 'templates/module', 'podcasts-compact' ); ?>
    </section>
  </div>
</div>
</div>
