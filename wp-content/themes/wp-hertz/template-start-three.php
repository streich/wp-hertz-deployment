<?php
/*
Template Name: Start
*/
?>
<div class="content" >
    <div class="container ">
        <div class=" row">
            <div class="col-md-12 slick-slider__container">
              <!-- <h5 class="section-headline"> Meldungen </h5> -->
          <?php get_template_part( 'templates/module', 'message-slider' ); ?>
            </div>
        </div>

        <section class="section-shows-start">
            <h5 class="section-headline"> Aktuelle Sendungen auf Hertz 87.9 </h5>
            <?php get_template_part( 'templates/module', 'shows-compact' ); ?>
        </section>
        <hr>
        <div class=" row">
            <div class="col-md-8">
              <h5 class="section-headline"> Nachrichten </h5>
              <?php get_template_part( 'templates/module', 'news-compact' ); ?>

            </div>
            <div class="col-md-4">

              <h5 class="section-headline"> Veranstaltungen </h5>
              <?php get_template_part( 'templates/module', 'events-compact' ); ?>

                <div
            </div>
        </div>
    </div>
</div>
<div class="content hidden" >
    <div class="container">
        <hr>
        <section class="section-podcasts-start ">
            <h5 class="section-headline"> Podcasts </h5>
            <div class="row">
                <?php get_template_part( 'templates/module', 'podcasts-compact' ); ?>
            </section>
        </div>
    </div>
</div>
