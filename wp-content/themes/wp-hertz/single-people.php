<div class="content content--white">
  <div class="container">
    <?php while (have_posts()) : the_post();

        if(has_post_thumbnail($post->ID)){
        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'people-full', false, '' );
        $image = $src[0];         // Medium resolution
        }else{
        $image = false;
        }
    ?>


    <article class="row article-people-detail">
      <div class="col-sm-5">
        <img class="article-people__image" style="margin-top: 50px" src="<?php echo $image; ?>">
        <?php get_template_part('templates/element', 'cc'); ?>

      </div>
      <div class="col-sm-6">
        <header>
          <div class="breadcrumbs">
            <?php the_breadcrumb(); ?>
          </div>
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>

        <div class="article-people__content">
          <?php echo wpautop($post->post_content); ?>
        </div>

        <?php if(get_field('playlist') ): ?>
        <div class="section-meta__links">
          <h5 class="section-headline">Meine Playlist</h5>
          <ul class="linklist">

            <?php while( have_rows('playlist') ): the_row();
                // vars
                $title = get_sub_field('song');
                $name = get_sub_field('song');
                $url = get_sub_field('link');
                if(!$name) $name = $url;

                ?>
            <li class="linklist__item">
              <svg class=""
                style="width:16px; height:16px; opacity: 0.3; margin-right: 10px; margin-bottom: -3px; float: left">
                <use xlink:href="<?php echo theme_root(); ?>/build/svg/icons.svg#icon-hertz-dark"></use>
              </svg>
              <?php if( $url ): ?>
              <a target="_blank" style="display: block" href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                <?php echo $name; ?>
              </a>
              <?php else: ?>
              <?php echo $name; ?>
              <?php endif; ?>

            </li>
            <?php endwhile; ?>
          </ul>
        </div>
        <?php endif; ?>
      </div>
    </article>
    <?php endwhile; ?>

  </div>
</div>