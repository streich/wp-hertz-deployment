<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$cats = wp_get_post_terms($post->ID,'show');
foreach($cats as $cat){
  $category_slug = $cat->slug;
  $category_name = $cat->name;
}
$icon_object = get_field('show_image', $cats[0]);
if($icon_object)
	$icon = $icon_object["url"];
else
	$icon = get_template_directory_uri()."/build/img/show_general.png";


if(has_post_thumbnail($post->ID)){
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), "page-full", false, '' );
	$image = $src[0];         // Medium resolution
}elseif ( get_field('show_placeholder', $cats[0])){
	$image =  get_field('show_placeholder', $cats[0])['sizes']['page-full'];
}elseif ( get_field('show_image', $cats[0])){
	$image = get_field('show_image', $cats[0])->url;
}


$file = get_field('file', $post);
$files = get_field('files', $post);
$context['icon'] = $icon;
$context['links'] = get_field('links', $post);
$context['image'] = $image;
$context['file'] = $file;
$context['files'] = $files;
$context['hasFile'] = !!$file;
$context['hasFiles'] = !!$files;
$context['noImage'] = !!!$image;
if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
?>

