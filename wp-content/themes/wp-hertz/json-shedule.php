<?php
// include our wordpress functions
// change relative path to find your WP dir
define('WP_USE_THEMES', false);
require('../../../wp-blog-header.php');

// set header for json mime type
header('Content-type: application/json;');

// get latest single post
// exclude a category (#5)
query_posts(array(
  'posts_per_page' => 1,
  'cat' => -5,
));


$complete_schedule = getScheduleArray();


$days_map = array(
  "Montag" => "1",
  "Dienstag" => "2",
  "Mittwoch" => "3",
  "Donnerstag" => "4",
  "Freitag" => "5",
  "Samstag" => "6",
  "Sonntag" => "7"
  );

$type_map = ["wöchentlich","zweiwöchentlich","unregelmäßig","einmal im Monat"];

  // print_r($complete_schedule)
function getScheduleArray(){
    $days = array(
    "1" => [],
    "2" => [],
    "3" => [],
    "4" => [],
    "5" => [],
    "6" => [],
    "7" => []
  );
  return $days;

}



$jsonpost = array();

$taxonomy     = 'show';
$orderby      = 'name';
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';

$args = array(
  'orderby'      => $orderby,
  'show_count'   => $show_count,
  'pad_counts'   => $pad_counts,
  'hierarchical' => $hierarchical,
  'hide_empty' => 0
);

$terms = get_terms($taxonomy,$args);

foreach($terms as $term){
  $schedule = get_field('schedule', $term);

  if( have_rows('schedule',$term) ):
    // loop through the rows of data
    while ( have_rows('schedule',$term) ) : the_row($term);
      // add new event to time
      $hour_start = get_sub_field('hour');
      $hour_end = get_sub_field('hour_end');
      if($hour_end < 3 && $hour_end < $hour_start){
         $hour_end = $hour_end + $hour_start +1 ;
      }

        $event = array(
          "name"=>$term->name,
          "url"=> get_term_link($term)."&post_type=shows",
          // "hour" => $hour_start,
          "desc" => $term->description,
          "time" =>  $hour_start.':'.sprintf("%02d", get_sub_field('minute')).' - '. $hour_end.':'.sprintf("%02d", get_sub_field('minute_end'))
          // "schedule_type" => $type_map[(int)get_field("schedule_type",$term)],
          // "from" => $hour_start.':'.sprintf("%02d", get_sub_field('minute')),
          // "to" => $hour_end.':'.sprintf("%02d", get_sub_field('minute_end'))
        );

        array_push($complete_schedule[$days_map[get_sub_field('day')]],$event);

       // echo "<hr>";
    endwhile;
  endif;

}


// output json to file
echo json_encode($complete_schedule);
?>
