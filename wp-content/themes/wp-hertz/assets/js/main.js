'use strict';
/* jscs: disable */
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
// var $ = jQuery.noConflict();


!function(e,t,n,o,i){function r(t,n){if(n){var o=n.getAttribute("viewBox"),i=e.createDocumentFragment(),r=n.cloneNode(!0);for(o&&t.setAttribute("viewBox",o);r.childNodes.length;)i.appendChild(r.childNodes[0]);t.appendChild(i)}}function a(){var t=this,n=e.createElement("x"),o=t.s;n.innerHTML=t.responseText,t.onload=function(){o.splice(0).map(function(e){r(e[0],n.querySelector("#"+e[1].replace(/(\W)/g,"\\$1")))})},t.onload()}function d(){for(var i;i=t[0];){var s=i.parentNode,l=i.getAttribute("xlink:href").split("#"),u=l[0],c=l[1];if(s.removeChild(i),u.length){var m=o[u]=o[u]||new XMLHttpRequest;m.s||(m.s=[],m.open("GET",u),m.onload=a,m.send()),m.s.push([s,c]),4===m.readyState&&m.onload()}else r(s,e.getElementById(c))}n(d)}i&&d()}(document,document.getElementsByTagName("use"),window.requestAnimationFrame||window.setTimeout,{},/Trident\/[567]\b/.test(navigator.userAgent));

var DAYS = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag','Sonntag'];
var MONTHS = ['Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember'];



var utils = {};
(function($) {
    var SUPERSTART = {
        // All pages
        common: {
            init: function() {


              function HertzAudioStream(url) {
                var _this = {};

                //Create the audio tag
                var soundFile = document.createElement('audio');
                soundFile.preload = 'auto';
                  //Load the sound file (using a source element for expandability)
                  var src = document.createElement('source');
                  soundFile.appendChild(src);
                  function _getState(){
                    return sessionStorage.getItem('hertzStreamState');
                  }
                    _this.init = function () {
                        _this.update('init');
                    };

                    _this.update = function (state) {
                        if(!state) state = sessionStorage.getItem('hertzStreamState');
                        switch(state){
                          case 'init':
                                if(_getState() === 'playing'){
                                  _this.play();
                                }
                                break;
                          case 'playing':
                            _this.pause();
                            break;
                          case 'pause':
                            _this.play();
                            break;
                          default:
                            _this.play();
                            break;
                        }
                    };

                    _this.setState = function(state) {
                      sessionStorage.setItem('hertzStreamState',state);
                        $('.stream-player').attr('class',
                           function(i, c){
                              return c.replace(/(^|\s)stream-player--state-\S+/g, '');
                           });
                          $('.stream-player').addClass('stream-player--state-'+state);
                        // switch(state){
                        //   case 'loading': _setClass('loading'); break;
                        //   case 'loading': _setClass(loading); break;
                        // }
                    };
                    _this.play = function () {
                      _this.setState('loading');
                      soundFile.addEventListener('playing',function(){
                        setTimeout(function(){
                       _this.setState('playing');
                     },500);
                      });
                     soundFile.addEventListener('waiting',function(){
                      _this.setState('loading');
                      });
                      if(!src.src || src.src === '') {
                        src.src = url;
                        soundFile.load();
                      }
                      //Load the audio tag
                      //It auto plays as a fallback

                      soundFile.volume = 0;
                      soundFile.play();


                      soundFile.volume = 1;
                       //Due to a bug in Firefox, the audio needs to be played after a delay
                       setTimeout(function(){soundFile.play();},1);

                     };
                    _this.pause = function () {
                      _this.setState('pause');
                    soundFile.pause();

                };

                return _this;
                }

              window.liveStream = new HertzAudioStream('https://stream.radiohertz.de/hertz-hq.mp3');
              window.liveStream.init();
              $('.stream-player').on('click',function(e){
                e.stopPropagation();
                e.preventDefault();
                window.liveStream.update();
              });


              // Audiostream
              $.each($('.dropdown-menu-inner'),function(){
                var count = $(this).children().length;
                // console.log('count',count);
                if(count < 4){
                  $(this).children().first().addClass('sub-menu--offset-'+(4-count));
                    // console.log( $(this).children().first());
                }

              });

                $('.js-match-height').matchHeight();

                function addHeaderMargin(){
                  var height = $(".nav-main > .open section").outerHeight();
                  $(".head").css("margin-bottom",height - 15  +"px");
                }
                function removeHeaderMargin(){
                    $(".head").css("margin-bottom","0");

                }
                 $(".nav-main section").on("click", function(e){e.stopPropagation();});
                 $(".nav-main > li > a:not(.dropdown-toggle)").on("click", function(e){
                    removeHeaderMargin();
                 });

                 $(".nav-mega > a").click(function(e){
                    hideSearch();
                    if($(window).width() > 770){
                      setTimeout(function(){
                        addHeaderMargin();
                        $(document).one("click",removeHeaderMargin);
                      },10);
                    }
                });


              var delay = 0;
              function updateSong(){
                setTimeout(function(){
                 utils.getCurrentSong(function(song){
                    $(".currently-playing__song__artist").text(song[0]);
                    $(".currently-playing__song__track").text(song[1]);
                      updateSong();
                      // store song in local storage
                      if(localStorage){
                        localStorage.setItem('artist', song[0]);
                        localStorage.setItem('track', song[1]);
                      }
                    delay = 30000;
                  });
                },delay)

              }
              updateSong();

              function _initSlider(){};

              $('.slider').slick(
                {
                  dots: true,
                  infinite: true,
                  speed: 300,
                  slidesToShow: 1,
                  adaptiveHeight: true,
                  autoplay: true,
                  autoplaySpeed: 222000
                });


              function hideSearch(){
                $(".head-search").removeClass("in");

              }
              $(".js-toggle-search").click(function(){
                setTimeout(function(){
                    $(".js-search-input")[0].focus();
                  },100);
              });


               $('.tab-panel__tab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
                $(this).parent().addClass("active");
                $(this).parent().siblings(".active").removeClass("active");

              });


            }
        },
        home: {
            init: function() {}
        },
        page_template_template_schedule_php: {
            init: function() {
              // $(".tt_timetable").stacktable();
           }
        },
        page_template_template_playlist_php: {
            init: function() {
              var date = new Date();
              var today = new Date();
              var yesterday = new Date(today);
              yesterday.setDate(today.getDate() - 1);
              renderPlaylist(today);


            function renderPlaylist(date){
              if(!date) date= today;
                  $(".js-headline-date").text(utils.dateToReadbleString(date));

              if(date ==  today){
                $(".js-playlist-target").addClass("today");
              }else{
                $(".js-playlist-target").removeClass("today");

              }
              $(".js-playlist-target").empty();
               utils.getPlaylist(date.getFullYear(), date.getMonth()+1, date.getDate(), function(data){
                var template = Hogan.compile("<div class='item-playlist {{classes}}'><div class='container'><span class='time'>{{time}}</span> <b class='artist'>{{artist}}</b> <span class='track'>{{title}}</span></div></div>");


                for(var i=data.length-1; i>=0; i-- ){
                 $(".js-playlist-target").append(template.render(data[i]));
                };
              });

            }


             // DATEPICKER
              $(".js-playlist-date-set-today").click(function(){
                  $('input.js-playlist-date-input').datepicker('setDate',today);
              });
              $(".js-playlist-date-set-yesterday").click(function(){
                  $('input.js-playlist-date-input').datepicker('setDate',yesterday);
              });
              $(".js-playlist-date-show-datepicker").click(function(){
                  $('input.js-playlist-date-input').datepicker('show');
              });
              $('input.js-playlist-date-input').datepicker({
                    endDate: today,
                    calendarWeeks: true,
                    autoclose: true,
                    todayHighlight: true
                }).on("changeDate",function(e){
                  renderPlaylist(e.date);

                });

            }
        }
    };
    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var namespace = SUPERSTART ;
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            UTIL.fire('common');
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
            });
        }
    };
    $(document).ready(UTIL.loadEvents);

utils.getCurrentSong = function(callback){
    if(!TEMPLATE_DIRECTORY) TEMPLATE_DIRECTORY = "/wp-content/themes/wp-superstart";
      var proxy = TEMPLATE_DIRECTORY+'/ba-simple-proxy.php',
        url = proxy + '?send_session=0&user_agent=0&mode=0&full_status=1&full_headers=1&send_cookies=1&url=' + encodeURIComponent('http://stream.radiohertz.de/playlist/current.vorbiscomment');
     $.getJSON( url, function(data){
      var contents = data.contents.substr(data.contents.indexOf('artist='),data.contents.length);

      var splitted = contents.split("\n");
      var artist = splitted[0].match(/artist=(.*)/)[1].replace('[Hertz 87,9] ','');
      var song = splitted[1].match(/title=title=(.*)/)[1];
      callback([artist,song]);

      });
}

utils.getPlaylist = function(year,month,day,callback){
    if(!TEMPLATE_DIRECTORY) TEMPLATE_DIRECTORY = "/wp-content/themes/wp-superstart";
      var proxy = TEMPLATE_DIRECTORY+'/ba-simple-proxy.php',
        url = proxy + '?send_session=0&user_agent=0&mode=0&full_status=1&full_headers=1&send_cookies=1&url=' + encodeURIComponent('http://stream.radiohertz.de/playlist/'+year+'/'+utils.zeroFill(month,2)+'/'+utils.zeroFill(day,2)+'.xspf');
     $.getJSON( url, function(data){
        // console.log(data.contents);
        var contents = data.contents.substr(data.contents.indexOf('<?xml version'),data.contents.length);
        var xmldom = XSPF.XMLfromString(contents);

        var jspf = XSPF.toJSPF(xmldom);
        var data  = [];
        var classes = "";
        var i=0;
        $.each(jspf.playlist.track,function(){
            var timestamp =  this.meta[0]["http://stream.radiohertz.de/xspf/timestamp"];
            var dateTime = new Date(timestamp);
            // console.log(timestamp + " - " + this.creator + " - " + this.title);
            classes = "";
            if(i==0) classes = classes+" current";
            if(this.creator == "Event") classes = classes+" event";

            data.push({date: dateTime,
              time: utils.getFormattedTime(dateTime),
              artist: this.creator,
              classes: classes,
              title: this.title});
          i++;
        });
        callback(data);

      });
}
utils.dateToReadbleString = function( date ){
  return date.getDate() + ". " + MONTHS[date.getMonth()] + " " + date.getFullYear();
}
utils.getFormattedTime = function( date ){
  return utils.zeroFill(date.getHours(),2)+":"+utils.zeroFill(date.getMinutes(),2);
}
utils.zeroFill = function( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

})(jQuery); // Fully reference jQuery after this point.
