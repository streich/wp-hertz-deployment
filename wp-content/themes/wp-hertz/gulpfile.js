var gulp = require('gulp');
   concat = require('gulp-concat'),
   copy = require('gulp-copy'),
   uglify = require('gulp-uglify'),
   imagemin = require('gulp-imagemin'),
   sourcemaps = require('gulp-sourcemaps'),
   sass = require('gulp-ruby-sass'),
   livereload = require('gulp-livereload'),

   prefix = require('gulp-autoprefixer');
 var filesize = require('gulp-filesize');

var browserSync = require('browser-sync');
var reload      = browserSync.reload;


var del = require('del');

// "localhost:8000" caused problems so we had to add "wordpress" to the hosts file
var localhost = "127.0.0.1:8000";

var paths = {
  scripts: [
    // 'bower_components/jquery/dist/jquery.js',
    // 'bower_components/mediaelement/build/mediaelement-and-player.js',
    // 'bower_components/audiojs/audiojs/audio.js',
    // 'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tooltip.js',
    // 'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tranisiton.js',
    // 'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
    'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
    'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tranisiton.js',
    'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/collapse.js',
    'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/dropdown.js',
    'bower_components/stacktable/stacktable.js',
    'bower_components/xspf/xspf_parser_runner_files/xspf_parser.js',
    'bower_components/matchHeight/jquery.matchHeight.js',
    'bower_components/slick.js/slick/slick.js',
    'bower_components/hogan/web/1.0.0/hogan.js',
    'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap/tab.js',
    'assets/js/*.js'
  ],
  svg: 'assets/svg/*.svg',
  sass: 'assets/sass/main.sass',
  sassAll: 'assets/sass/**/*.sass',
  images: 'assets/img/**/*'
};

// cleaning tasks
gulp.task('clean-scripts', function(cb) {
  del(['build/js'], cb);
});
gulp.task('clean-css', function(cb) {
  del(['build/css'], cb);
});
gulp.task('clean-images', function(cb) {
  del(['build/img'], cb);
});


var svgstore = require('gulp-svgstore')
var svgmin = require('gulp-svgmin')
gulp.task('svg', function () {
  return gulp.src('assets/svg/*.svg')
             .pipe(svgmin())
             .pipe(svgstore({ fileName: 'icons.svg', prefix: 'icon-' }))
             .pipe(gulp.dest('build/svg/'))
});

gulp.task('svg-copy', function () {
  return gulp.src('assets/svg/*.svg')
            .pipe(gulp.dest('build/svg/'))
});

//BrowserSync
gulp.task('browser-sync', function () {
    //declare files to watch
    var files = [
    'build/js/*.js',
    'build/css/*.css',
    'build/svg/*.svg',
    'build/img/*.{png,jpg,jpeg,gif,svg}',
    '**/*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
    //browsersync can't run PHP so we proxy external local server
      proxy: localhost
    });
});


// Minify and copy all JavaScript (except vendor scripts)
// with sourcemaps all the way down
gulp.task('scripts', ['clean-scripts'], function() {
  return gulp.src(paths.scripts)
    // .pipe(sourcemaps.init())
     .pipe(concat('all.js'))
     .pipe(gulp.dest('build/js'))
      .pipe(uglify())
      .pipe(concat('all.min.js'))
      .pipe(filesize())
    .pipe(gulp.dest('build/js'))
     .on('error', function (err) { console.log(err.message); });
});




gulp.task('sass',['clean-css'], function () {
  return sass(paths.sass)
    .on('error', function (err) { console.log(err.message); })
    .pipe(prefix("last 1 version", "> 1%", "ie 8", "ie 7"))
    .pipe(gulp.dest('build/css'));
});




// Copy all static images
gulp.task('images', ['clean-images'], function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5, svgoPlugins: [{removeViewBox: false}]}))
    .pipe(gulp.dest('build/img'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.sassAll, ['sass']);
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.svg, ['svg']);
  gulp.watch(paths.images, ['images']);
});




// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch',   'sass',  'svg', 'svg-copy', 'scripts', 'images','browser-sync']);
gulp.task('build', [ 'scripts', 'images', 'svg', 'svg-copy', 'sass']);
