<div class="slider">
    <?php
      $args = array (
        'post_type'              => 'events',
        'posts_per_page'         => '6',
        );
    //WordPress loop for custom post type
    $my_query = new WP_Query($args);
    while ($my_query->have_posts()) : $my_query->the_post(); ?>

        <?php   if(has_post_thumbnail($post->ID)){
          $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), "article-full", false, '' );
          $image = $src[0];         // Medium resolution
        }else{
          $image = false;
        } ?>

        <?php
            $view = get_field('view');
            $date = get_field('date');
            $time = get_field('time');
         ?>

            <?php if( $view == 'text' ){ ?>

            <div class="slider-item article-event-slide--text">
                    <div class="article-event-slide__inner">

                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <h3 class="article-event-slide__headline">
                <?php the_title(); ?>
                </h3></a>
                <time class="article-event-slide__datetime"> <?php echo $date; ?>
                     <?php if($time): ?>
                              / <?php echo $time; ?> Uhr
                    <?php endif; ?>
                 </time>
                <p>
                <?php the_excerpt(); ?>
                </p>
            </div>
            </div>
            <?php } else if( $view  == 'both' ){ ?>
                <div class="slider-item  article-event-slide--both">
                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <div class="article-event-slide__image-container">
                    <img src="<?php echo $image; ?>">
                </div>
                <div class="article-event-slide__inner">

                <h3 class="article-event-slide__headline">
                <?php the_title(); ?>
                </h3>
                </a>
                <time class="article-event-slide__datetime"> <?php echo $date; ?>
                 <?php if($time): ?>
                              / <?php echo $time; ?> Uhr
                    <?php endif; ?>
                    </time>
            </div>
            </div>
            <?php } else { ?>
                <div class="slider-item article-event-slide--image">
                                    <div class="article-event-slide__image-container">

               <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <img src="<?php echo $image; ?>">
                </a>
            </div>
            </div>

        <?php } ?>
    <?php endwhile;  wp_reset_query(); ?>

</div>
