<div class="row">

  <?php
  //WordPress loop for custom post type

  $args = array (
          'post_type'              => 'shows',
          'posts_per_page'         => '6',
          'orderby' => 'date',
          'order'   => 'DESC',
      );
  $my_query = new WP_Query($args);
  $i = 0;
  while ($my_query->have_posts()) : $my_query->the_post();?>

  <?php
     $cats = wp_get_post_terms($post->ID,'show');

      foreach($cats as $cat){
        $category_slug = $cat->slug;
        $category_name = $cat->name;
      }

      if( get_field('schedule') && is_array(get_field('schedule')[0])){
        $date = get_field('schedule')[0]['date'];
      }else{
        $date =  get_the_time("d. F Y");
      }

      $image = "none";
      $title = get_the_title();
      $link =  get_the_permalink();
      if(!get_field('show_image', $cats[0])){
        $icon = get_template_directory_uri()."/build/img/show_general.png";
      }else{
        $icon =  get_field('show_image', $cats[0])['url'];
      }
      if(has_post_thumbnail($post->ID)){
        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), "article-full", false, '' );
        $image = $src[0];         // Medium resolution
      }elseif ( get_field('show_placeholder', $cats[0])){
        $image =  get_field('show_placeholder', $cats[0])['sizes']['article-full'];
      }elseif ( get_field('show_image', $cats[0])){
        $image = get_field('show_image', $cats[0])->url;
      }



      $excerpt = get_the_excerpt();
      // $show_description = get_field('short_description', $cats[0]);
      $show_description = $cats[0]->description;

      echo "<div class='col-md-12 col-sm-12'><div class='article-show-column__container'>";

      include('item-show-compact.php');
      echo "</div></div>";


  endwhile;  wp_reset_query(); ?>
</div>
<div class="row">
  <div class="col-md-12">
    <a class="link-more-content" href="<?php echo get_post_type_archive_link("shows"); ?> ">Mehr Artikel</a>
  </div>
</div>
