 <?php
    $args = array (
        'post_type'              => 'news',
        'posts_per_page'         => '4'
    );
                                                        //WordPress loop for custom post type
    $my_query = new WP_Query($args);
    while ($my_query->have_posts()) : $my_query->the_post();
?>

<article class="article-news">
    <header class="article-news__header">
    <span class="date" style="font-size: 12px; color: #999"><?php echo get_the_time("d F Y"); ?> </span>
        <h3 class="article-news__headline">
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a>
        </h3>
    </header>
    <p class="article-news__text">
         <?php echo get_the_excerpt(); ?>
        <!--  <br>
         <a class="link-more" href="<?php the_permalink(); ?>">mehr →</a> -->
    </p>
<!--     <footer class="article-news__footer">

    </footer> -->

</article>

<?php endwhile;  wp_reset_query(); ?>
<!-- <a href="/?post_type=news">Alle Nachrichten</a> -->
<a class="link-more-content" href="<?php echo get_post_type_archive_link("news"); ?> ">Mehr Nachrichten</a>
<br>
