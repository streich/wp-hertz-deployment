
<article class="article-podcast">
	<header class="article-podcast__header">
		<h3 class="article-podcast-headline">
			<a href="<?php echo $podcast_link; ?> " rel="bookmark" title="<?php the_title_attribute(); ?>">
			 <?php echo $podcast_title; ?>
			</a>
		</h3>
	</header>
	<div class="article-podcast__body">
	  	<audio src="<?php echo $podcast_file; ?>"controls preload="none">
			<a href ="<?php echo $podcast_file; ?> ">Podcast herunterlden</a>
	  	</audio>
		<p class="article-podcast__description">
			<?php echo $podcast_description; ?>
		</p>
	</div>
	<footer class="article-podcast__footer">
		<?php if($category_name): ?>
		<a href="/?show=<?php echo $category_slug; ?>&post_type=podcasts">
			<?php echo $category_name; ?>
		</a>
		-
		<?php endif; ?>
		<?php echo $podcast_date; ?>
	</footer>
</article>
