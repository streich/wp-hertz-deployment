<article class="article-show-column   <?php if ( !$image ) echo 'article-show-column--no-image'?>">
  <header class="article-show-column__header">
  <?php if ( $icon ): ?>
    <a href="/?show=<?php echo $category_slug; ?>"><img class="article-show-column__icon" src="<?php echo $icon; ?>"/></a>
  <?php endif; ?>
  <?php if ( $image ):?>
  <a   href="<?php echo $link; ?> " class="article-show-column__image-container">
    <img class="article-show-column__image" src="<?php echo $image; ?>"/>
  </a>
  <?php endif; ?>
  </header>
  <div class="article-show-column__body">
    <h2 class="article-show-column__title"><a href="<?php echo $link; ?> " rel="bookmark" title="<?php $title_attribute; ?>"> <?php echo $title; ?></a></h2>
    <p class="article-show-column__text"><?php echo $excerpt; ?> </p>
  </div>
  <div class="article-show-column__footer">
    <a class="category" href="/?show=<?php echo $category_slug; ?>"><?php echo $category_name; ?></a>
    <span class="date"> am  <?php echo $date; ?> </span>
  <?php if ( $show_description != "" ): ?>
    / <?php echo $show_description; ?>
  <?php endif; ?>
  </div>
</article>
