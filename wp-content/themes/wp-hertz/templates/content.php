<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
	<time class="hidden updated" datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time>
  </div>
   <a class="link-more-content" href="<?php the_permalink(); ?>">Anzeigen </a>   
</article>
<hr>