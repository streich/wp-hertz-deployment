<?
// $podcast_date 	=  get_the_time("d M yy");
// $podcast_title	 = get_the_title();
// $podcast_link 	=  get_the_permalink();
// $podcast_file 	= get_field('file' );
// $podcast_description	 = get_field('description' );
?>
<header class="article-header">
	<h3 class="h2"><a href="<?php echo $podcast_link; ?> " rel="bookmark" title="<?php the_title_attribute(); ?>"> <?php echo $podcast_title; ?></a></h3>
</header>                
<div class="content">
	<p> 
		<?php echo $podcast_description; ?> 
	</p> 
</div>