  <?php
                      //WordPress loop for custom post type

  $args = array (
          'post_type'              => 'shows',
          'posts_per_page'         => '6',
      );
  $my_query = new WP_Query($args);
  while ($my_query->have_posts()) : $my_query->the_post(); ?>


  <?php

     $cats = wp_get_post_terms($post->ID,'show');

      foreach($cats as $cat){
        $category_slug = $cat->slug;
        $category_name = $cat->name;
      }

      $date =  get_the_time("d. F Y");
      $title = get_the_title();
      $link =  get_the_permalink();
      if(has_post_thumbnail($post->ID)){
        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 150,150 ), false, '' );
        $image = $src[0];         // Medium resolution
      }else{
        $image = get_field('show_image', $cats[0])["url"];
      }
      $excerpt = get_the_excerpt();
       $show_description = get_field('short_description', $cats[0]);

      include('item-show-preview.php');
  ?>      

  <?php endwhile;  wp_reset_query(); ?>

<a class="link-more-content" href="<?php echo get_post_type_archive_link("shows"); ?> ">Mehr Artikel</a>
