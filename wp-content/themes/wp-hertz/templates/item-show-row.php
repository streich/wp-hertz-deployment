<article class="article-show-column">
  <header class="article-show-column__header">
  <?php if ( $icon ): ?>
    <img class="article-show-column__icon" src="<?php echo $icon; ?>"/>
  <?php endif; ?>
  <a   href="<?php echo $link; ?> " class="article-show-column__image-container">
  <?php if ( $image ):?>
    <img class="article-show-column__image" src="<?php echo $image; ?>"/>
  <?php endif; ?>
  </a>
  </header>
  <div class="article-show-column__body">
    <h2 class="article-show-column__title"><a href="<?php echo $link; ?> " rel="bookmark" title="<?php $title_attribute; ?>"> <?php echo $title; ?></a></h2>
    <p class="article-show-column__text"><?php echo $excerpt; ?> </p>
  </div>
  <div class="article-show-column__footer">
    <a class="category" href="/?show=<?php echo $category_slug; ?>"><?php echo $category_name; ?></a>
    <span class="date"> am  <?php echo $date; ?> </span>
  <?php if ( $show_description != "" ): ?>
    / <?php echo $show_description; ?>
  <?php endif; ?>
  </div>
</article>
