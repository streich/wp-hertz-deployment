<?php
$cats = wp_get_post_terms($post->ID,'show');
if(has_post_thumbnail($post->ID)){
  $id = get_post_thumbnail_id();
}elseif ( get_field('show_placeholder', $cats[0])){
  $id=  get_field('show_placeholder', $cats[0])['id'];
}

$author_name = get_post_meta($id, 'be_photographer_name', true);
  $author_url = get_post_meta($id, 'be_photographer_url', true);
  $licence_name = get_post_meta($id, 'be_licence_name', true);
  $licence_url = get_post_meta($id, 'be_licence_url', true);

?>
<?php  if($author_name): ?>
 <p class="copyright">
   Bild:
   <?php  if($author_url): ?>
   <a target="_blank"  href="<?php echo $author_url; ?>">
     <?php echo $author_name; ?>
   </a>
 <?php else: ?>
     <span><?php echo $author_name; ?></span>
  <?php endif; ?>

   <?php  if($licence_name): ?>
    - Lizenz:
    <?php  if($licence_url): ?>
    <a target="_blank"  href="<?php echo $licence_url; ?>">
      <?php echo $licence_name; ?>
    </a>
  <?php else: ?>
      <span>
        <?php echo $licence_name; ?>
      </span>
  <?php endif; ?>
  <?php endif; ?>
  </p>

<?php endif; ?>
