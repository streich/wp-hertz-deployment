<?
?>
<article class="article-show">
	<?php if ( $image ) { ?>
		<img class="show-image show-image-left" src="<?php echo $image; ?>"/> 
	<?php } else { ?>
		<img class="show-image show-image-left" src="<?php bloginfo('template_directory'); ?>/build/img/show_general.png">
	<?php } ?>	

	<header class="article-show__header">
		<h2 class="article-show__title"><a href="<?php echo $link; ?> " rel="bookmark" title="<?php $title_attribute; ?>"> <?php echo $title; ?></a></h2>
	</header>                
	<div class="show-body">
		<p class="article-show__text"><?php echo $excerpt; ?> </p>
	</div>
	<?php if ( $show_description != "" ): ?>
	<div class="article-show__footer">
		<a class="category" href="/?show=<?php echo $category_slug; ?>"><?php echo $category_name; ?></a> 
		<span class="date"> am  <?php echo $date; ?> </span>
		/ <?php echo $show_description; ?>  
	</div>
	<?php endif; ?>	
</article>


