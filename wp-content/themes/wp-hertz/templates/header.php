
<?php $TEMPLATE_PATH = wp_make_link_relative(theme_root()); ?>
<header class="head"  role="banner" >
   <div class="container">
    <nav class=" navbar navbar-top" role="navigation">
      <div class="currently-playing navbar-right" onclick="window.open('<?php echo get_permalink(get_page_by_title('Playlist')); ?>','_self')">

        <div class="currently-playing__song">
          <div class="stream-player stream-player--state-idle " >

        <!-- 3  -->
        <div class="stream-player__svg stream-player__svg--loading" title="2">
          <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="20px" height="20px" viewBox="0 0 50 50" style="enable-background:new 0 0 30 30;" xml:space="preserve">
          <path fill="#000" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
            <animateTransform attributeType="xml"
              attributeName="transform"
              type="rotate"
              from="0 25 25"
              to="360 25 25"
              dur="0.6s"
              repeatCount="indefinite"/>
            </path>
          </svg>
        </div>

        <!-- 4 -->
        <div class="stream-player__svg stream-player__svg--playing" title="3">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 255 255 255 50;" xml:space="preserve">
            <rect x="4" y="0" width="3" height="7" fill="#333">
              <animateTransform  attributeType="xml"
                attributeName="transform" type="scale"
                values="1,1; 1,2; 1,1"
                begin="0s" dur="0.6s" repeatCount="indefinite" />
            </rect>

            <rect x="9" y="0" width="3" height="7" fill="#000">
              <animateTransform  attributeType="xml"
                attributeName="transform" type="scale"
                values="1,0.5; 1,2; 1,0.5"
                begin="0.2s" dur="0.6s" repeatCount="indefinite" />
            </rect>
            <rect x="14" y="0" width="3" height="7" fill="#333">

              <animateTransform attributeType="xml"
                attributeName="transform" type="scale"
                values="1,0.5; 1,2; 1,0.5"
                begin="0.4s" dur="0.6s" repeatCount="indefinite" />
            </rect>
          </svg>
        </div>


        <!-- PAUSE -->
        <div class="stream-player__svg stream-player__svg--pause" title="3">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 255 255 255 50;" xml:space="preserve">
            <rect x="2" y="0" width="6" height="16" fill="#000">
            </rect>

            <rect x="12" y="0" width="6" height="16" fill="#000">
            </rect>

          </svg>
        </div>
        <div class="stream-player__svg stream-player__svg--play" title="3">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 255 255 255 50;" xml:space="preserve">
             <polygon points="2,0 16,8 2,16"  />

          </svg>
        </div>
      </div>
            <div class="currently-playing__song__inner">
              <!-- <svg class="icon currently-playing__icon">
                  <use xlink:href="<?php echo $TEMPLATE_PATH; ?>/build/svg/icons.svg#icon-hertz"></use>
                </svg> -->
              <span id="artist" class="currently-playing__song__artist">
                 Aktueller Song
              </span>
              /
              <span  id="track"  class="currently-playing__song__track">
               Aktueller  Titel
              </span>
            </div>
                  <?php
                    if (has_nav_menu('header_navigation')) :
                      wp_nav_menu(array('theme_location' => 'current_track_navigation','container' => '', 'menu_class' => 'currently-playing__submenu'));
                    endif;

                  ?>
            </div>
          </div>
          <script>
           if(localStorage){
              document.getElementById('artist').innerHTML = localStorage.getItem('artist');
              document.getElementById('track').innerHTML =  localStorage.getItem('track');
            }
          </script>
          <?php
            if (has_nav_menu('header_navigation')) :
              wp_nav_menu(array('theme_location' => 'header_navigation','container' => '', 'menu_class' => 'nav nav-top  hidden-xs navbar-nav  navbar-right'));
            endif;
          ?>
        </nav>
        <div class="navbar-header ">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Navigieren</span>
              <svg class="icon ">
                  <use xlink:href="<?php echo $TEMPLATE_PATH; ?>/build/svg/icons.svg#icon-hamburger"></use>
                </svg>
          </button>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".head-search">
              <span class="sr-only">Suchen</span>
              <svg class="icon ">
                  <use xlink:href="<?php echo $TEMPLATE_PATH; ?>/build/svg/icons.svg#icon-search"></use>
                </svg>
          </button>
            <a class="head-logo" href="<?php echo esc_url(home_url('/')); ?>">
                <svg class="visible-sm-inline-block visible-xs-inline-block" style="">
                    <use xlink:href="<?php echo $TEMPLATE_PATH; ?>/build/svg/icons.svg#icon-logo"></use>
                </svg>
                  <svg class="hidden-sm hidden-xs" style="">
                    <use xlink:href="<?php echo $TEMPLATE_PATH; ?>/build/svg/icons.svg#icon-logo-subtitle"></use>
                </svg>
            </a>

        </div>

        <nav class="collapse navbar navbar-main navbar-collapse collapsed   " role="navigation">

          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'container' =>'', 'menu_class' => 'nav  nav-main navbar-nav', 'walker' => new megaMenuWalker()));
            endif;
          ?>


        </nav>
          <div class="head-search fade collapse">
              <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                <input type="text" placeholder="Suchbegriff eingeben..." class="js-search-input head-search__textfield" value="" name="s" id="s"  />
                <!-- <input type="submit" id="searchsubmit" value="Search"  /> -->
                <button class="head-search__button head-search__button-submit">
                   <svg class="icon-24">
                      <use xlink:href="<?php echo $TEMPLATE_PATH;?>/build/svg/icons.svg#icon-arrow-right"></use>
                    </svg>
                </button>
              </form>
          </div>


      </div>
    </div>

</header>
