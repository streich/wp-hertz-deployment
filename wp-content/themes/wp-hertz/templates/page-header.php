<div class="page-header">
  <div class="breadcrumbs">
        <?php the_breadcrumb(); ?>
  </div>
  <h1>
    <?php echo the_title(); ?>
  </h1>
</div>
