  <?php
  $args = array (
          'post_type'              => 'linklist',
          'posts_per_page'         => '12',
      );
  $my_query = new WP_Query($args);
  $i = 0;
  while ($my_query->have_posts()) : $my_query->the_post();?>

  <?php

      if( get_field('link_url')){
        $link = get_field('link_url');
      }
      if( get_field('description')){
        $description = get_field('description');
      }
      $title = get_the_title();

      ?>



      <article class="article-news">
    <header class="article-news__header">
    <span class="date" style="font-size: 12px; color: #999"><?php echo get_the_time("d F Y"); ?> </span>
        <h3 class="article-news__headline">
        <a href="<?php echo $link; ?>" target="_blank" rel="bookmark" title="<?php $title; ?>"><?php echo $title; ?> </a>
        </h3>
    </header>
    <?php if($description) : ?>

    <p class="article-news__text">
         <?php echo $description; ?>
         </p>
<?php endif; ?>
    </article>
  <?php

  endwhile;  wp_reset_query(); ?>
