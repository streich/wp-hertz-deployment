<div class="slider">
  <?php
      $args = array (
        'post_type'              => 'messages',
        'posts_per_page'         => '6',
        );
    //WordPress loop for custom post type
    $my_query = new WP_Query($args);
    while ($my_query->have_posts()) : $my_query->the_post(); ?>

  <?php   if(has_post_thumbnail($post->ID)){
  $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), "page-full", false, '' );
  $image = $src[0];         // Medium resolution
}else{
  $image = false;
} ?>

  <?php
    $view = get_field('view');
  ?>

  <?php if( $view == 'text' ){ ?>

  <div class="slider-item article-message-slide article-message-slide--text">
    <a href="<?php the_permalink(); ?>" rel="bookmark">
      <div class="article-message-slide__inner">
        <h3 class="article-message-slide__headline">
          <?php the_title(); ?>
        </h3>
        <p>
          <?php the_excerpt(); ?>
        </p>
      </div>
    </a>
  </div>
  <?php } else if( $view  == 'both' ){ ?>
  <div class="slider-item  article-message-slide article-message-slide--both">
    <a href="<?php the_permalink(); ?>" rel="bookmark">

      <div class="article-message-slide__image-container">
        <img src="<?php echo $image; ?>">
      </div>
      <div class="article-message-slide__inner">
        <h3 class="article-message-slide__headline">
          <?php the_title(); ?>
        </h3>
      </div>
    </a>
  </div>
  <?php } else if( $view  == 'all' ){ ?>
  <div class="slider-item article-message-slide  article-message-slide--all">
    <a href="<?php the_permalink(); ?>" rel="bookmark">
      <div class="article-message-slide__image-container">
        <img src="<?php echo $image; ?>">
      </div>
      <div class="article-message-slide__inner">
        <h3 class="article-message-slide__headline">
          <?php the_title(); ?>
        </h3>
        <p>
          <?php echo  get_the_excerpt(); ?>
        </p>
      </div>
    </a>
  </div>
  <?php } else { ?>
  <div class="slider-item article-message-slide article-message-slide--image">
    <a href="<?php the_permalink(); ?>" rel="bookmark">
      <div class="article-message-slide__image-container">
        <img src="<?php echo $image; ?>">
      </div>
    </a>
  </div>
  <?php } ?>
  <?php endwhile;  wp_reset_query(); ?>
</div>