<?php
    $args = array (
        'post_type'              => 'events',
        'posts_per_page'         => '10',
    );
                                                        //WordPress loop for custom post type
    $my_query = new WP_Query($args);
    while ($my_query->have_posts()) : $my_query->the_post();

    $date = get_field('date');
    $time = get_field('time');
?>

<div class="article-events--compact">
    <h3  class="article-events--compact__headline" >
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
        <?php the_title(); ?>
        </a>
    </h3>
            <time class="article-events--compact__datetime">  <b><?php echo $date; ?> </b>
                <?php if($time): ?>
                / <?php echo $time; ?> Uhr
                <?php endif; ?>

            </time>
       <!-- <a class="link-more" href="<?php the_permalink(); ?>" title="mehr Informationen">mehr Infos →</a>    -->
</div>

<?php endwhile;  wp_reset_query(); ?>
<a class="link-more-content" href="/?post_type=events">Alle Veranstaltungen</a>
