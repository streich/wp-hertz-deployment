<?php
    $args_podcasts = array (
        'post_type'        => 'podcasts',
        'posts_per_page'         => '6',
);
$wp_query_podcasts = new WP_Query($args_podcasts);
while ($wp_query_podcasts->have_posts()) : $wp_query_podcasts->the_post(); ?>
<div class="col-md-4 col-sm-6 col-xs-12">
<?php
        $cats = wp_get_post_terms($post->ID,'show');
         foreach($cats as $cat){
            $category_slug = $cat->slug;
            $category_name = $cat->name;
        }
        $podcast_date =  get_the_time("d M yy");
        $podcast_title = get_the_title();
        $podcast_link =  get_the_permalink();
        $podcast_file = get_field('file' );
        $podcast_description = get_the_excerpt();
?>
<?php include('item-podcast.php'); ?>
</div>
<?php endwhile;  ?>
</div>
<!-- <a href="/?post_type=podcasts">Alle Podcasts</a> -->

<a class="link-more-content" href="<?php echo get_post_type_archive_link("podcasts"); ?> ">Mehr Podcasts</a>
