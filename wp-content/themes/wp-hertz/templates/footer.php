<footer class="footer" role="contentinfo" >
	<div class="container">
		<div class="row">
		  	<div class="col-md-3  col-sm-3 col-xs-6">
			    <svg class="footer__logo">
			      	<use xlink:href="<?php echo theme_root();?>/build/svg/icons.svg#icon-logo-white"></use>
			    </svg>
				<?php dynamic_sidebar('sidebar-footer-a'); ?>
		  	</div>
		  	<div class="col-md-2 col-md-offset-1 col-sm-3 col-xs-6">
		    	<?php dynamic_sidebar('sidebar-footer-b'); ?>
		  	</div>
		  	<div class="col-md-2 col-sm-3 col-xs-6">
		    	<?php dynamic_sidebar('sidebar-footer-c'); ?>
		  	</div>
		  	<div class="col-md-2 col-sm-3 col-xs-6">
		    	<?php dynamic_sidebar('sidebar-footer-d'); ?>
		  	</div>
	  	  	<div class="col-md-2 col-sm-3 col-xs-6">
		    	<?php dynamic_sidebar('sidebar-footer-e'); ?>
		  	</div>
		</div>
	</div>
</footer>
