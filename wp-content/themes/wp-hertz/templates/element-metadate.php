<div class="section-meta__date">
    <h5 class="section-headline">Datum</h5>
    <ul class="datelist">
        <li class="datelist__item">
          <time><span class="datelist__date"> <?php echo get_the_date(); ?></span></time>
        </li>
    </ul>
</div>
