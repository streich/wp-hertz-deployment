<?php
/*
Template Name: Search Page
*/
?>
<div class="  page-header--fullwidth">
  <div class="container">
    <div class="breadcrumbs">

      <?php the_breadcrumb(); ?>
    </div>
    <h1> Suchergebnisse</h1>
  </div>
</div>


<div class="content">
  <div class="container" style="padding-top:20px">
    <?php if (!have_posts()) : ?>
    <div class="article-not-found">
      <?php _e('Sorry, nichts gefunden.'); ?>
    </div>
    <?php endif; ?>

    <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', get_post_format()); ?>
    <?php endwhile; ?>

    <?php if ($wp_query->max_num_pages > 1) : ?>
    <nav class="post-nav">
      <ul class="pager">
        <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
        <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
      </ul>
    </nav>
    <?php endif; ?>

  </div>
</div>