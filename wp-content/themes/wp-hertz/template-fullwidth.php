<?php
/*
Template Name: Template Content Fullwidth
*/
?>


<div class="content page-header--fullwidth">
  <div class="container  page-header__inner" role="document">

    <div class="breadcrumbs">
      <?php the_breadcrumb(); ?>
    </div>

    <h1>
      <?php echo the_title(); ?>
    </h1>

    <?php
			$page_description = get_the_excerpt();
			if($page_description)
			{
				echo '<p class="page_description">'.$page_description.'</p>';
			}
		?>
  </div>
</div>
<div class="content content--white page-content">
  <div class="container page-content__inner">
    <?php echo the_content(); ?>
  </div>
</div>